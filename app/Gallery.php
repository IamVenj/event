<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Str;

use Storage;

class Gallery extends Model
{
    protected $fillable = ['name', 'image', 'location'];

    function add_file($name, $image, $location)
    {

   
		$file = Str::random(30).'.'.$image->getClientOriginalExtension();

		$this::create([

    		'name' => $name,

    		'image' => $file,

    		'location' => $location

    	]);

    	Storage::putFileAs('public/uploads/gallery', $image, $file);
    	

    }

    function update_file($name, $location, $id)
    {
    	$gallery = $this::find($id);

    	$gallery->name = $name;

    	$gallery->location = $location;

    	$gallery->save();
    }

    function update_image($file_req, $id)
    {
    	$gallery = $this::find($id);

    	$file = Str::random(30).'.'.$file_req->getClientOriginalExtension();

    	Storage::delete('public/uploads/gallery/'.$gallery->file);

    	$gallery->image = $file;

    	$gallery->save();

    	Storage::putFileAs('public/uploads/gallery', $file_req, $file);
    }

    function delete_file($id)
    {
    	$gallery = $this::find($id);

    	if(!is_null($gallery->image))
    	{
    		Storage::delete('public/uploads/gallery/'.$gallery->image);
    	}

    	$gallery->delete();
    }


}
