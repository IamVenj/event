<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = ['name', 'email', 'subject', 'message'];

    function create_contact($name, $email, $subject, $message)
    {
    	$this::create([

    		'name' => $name,

    		'email' => $email,

    		'subject' => $subject,

    		'message' => $message

    	]);
    }
}
