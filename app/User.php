<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

use Storage;

use Str;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','phone_number', 'gender', 'role_id', 'profile_picture'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function create_user($name, $email, $mobile_number, $password, $role, $gender)
    {

        $this::create([

            'name' => $name,

            'email' => $email,

            'phone_number' => $mobile_number,

            'password' => $password,

            'role_id' => $role,

            'gender' => $gender

        ]);

    }

    public function update_profile($name, $email, $gender, $profile_picture, $phone_number, $id)
    {
        $profile = $this::find($id);

        if(!is_null($profile_picture))
        {

            $image = Str::random(30).'.'.$profile_picture->getClientOriginalExtension();
            
            if(!is_null($profile->profile_picture))
            {
                Storage::delete('public/uploads/user/profile/'.$profile->profile_picture);
            }

            Storage::putFileAs('public/uploads/user/profile', $profile_picture, $image);

            $profile->profile_picture = $image;
            
        }

        $profile->name = $name;

        $profile->gender = $gender;

        $profile->phone_number = $phone_number;

        $profile->save();

    }

    function _update_account_($name, $email, $password, $id)
    {
        $user = $this::find($id);

        if(!is_null($password))
        {
            $user->name  = $name;

            $user->email  = $email;

            $user->password = bcrypt($password);
        }
        else
        {
            $user->name = $name;

            $user->email = $email;
        }
    }

    public function isConnected()
    {
        $connected = @fsockopen("www.google.com", 80);

        if($connected)
        {
            return true;
        }
        else{
            return false;
        }
    }

}
