<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserEvent extends Model
{
    protected $fillable = ['user_id', 'event_id'];

    public function register_event($user_id, $event_id)
    {
		
    	$this::create([

    		'user_id' => $user_id,

    		'event_id' => $event_id

    	]);

    }

}
