<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = ['icon', 'title', 'slug'];

    public function create_service($icon, $title, $slug)
    {

		$this::create([

    		'icon' => $icon,

    		'title' => $title,

    		'slug' => $slug,

    	]);

    }

    public function delete_service($id)
    {

    	$service = $this::find($id);

    	$service->delete();

    }

    public function update_service($icon, $title, $slug, $id)
    {

    	$service = $this::find($id);

    	$service->icon = $icon;

    	$service->title = $title;

    	$service->slug = $slug;

    	$service->save();

    }

}
