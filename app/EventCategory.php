<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Storage;

use Str;

class EventCategory extends Model
{
    protected $fillable = ['name', 'image'];

    public function create_category($name, $image_request)
    {
    	if(!is_null($image_request))
    	{
    		$image = Str::random(30).'.'.$image_request->getClientOriginalExtension();

    		Storage::putFileAs('public/uploads/event-category', $image_request, $image);
	    	
	    	$this::create([

	    		'name' => $name,

	    		'image' => $image

	    	]);
    	}
    	else
    	{
    		$this::create([

	    		'name' => $name,

	    	]);
    	}

    }

    public function update_category($name, $id)
    {
    	$category = $this::find($id);

    	$category->name = $name;

    	$category->save();

    }

    public function update_category_image($image_request, $id)
    {
    	$category = $this::find($id);

    	$image = Str::random(30).$image_request->getClientOriginalExtension();

    	if(!is_null($category->image))
    	{
    		Storage::delete('public/uploads/event-category/'.$category->image);
    	}

    	Storage::putFileAs('public/uploads/event-category', $image_request, $image);

    	$category->image = $image;

    	$category->save();

    }

    public function destroy_category($id)
    {
    	$category = $this::find($id);

    	if(!is_null($category->image))
    	{
    		Storage::delete('public/uploads/event-category/'.$category->image);
    	}

    	$category->delete();

    }
}
