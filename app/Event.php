<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Str;

use Storage;

class Event extends Model
{
    protected $fillable = ['name', 'date_from', 'date_to', 'time', 'image', 'slug', 'location', 'map_location', 'price'];

    public function create_event($name, $date_from, $date_to, $time, $image_request, $slug, $location, $map_location, $price)
    {

    	if(!is_null($image_request))
    	{
    		$image = Str::random(30).'.'.$image_request->getClientOriginalExtension();

    		Storage::putFileAs('public/uploads/event', $image_request, $image);

    		$this::create([

	    		'name' => $name,

	    		'date_from' => $date_from,

	    		'date_to' => $date_to,

	    		'time' => $time,

	    		'image' =>$image,

	    		'slug' => $slug,

	    		'location' => $location,

	    		'map_location' => $map_location,

	    		'price' => $price

	    	]);
    	}
    	else
    	{
    		$this::create([

	    		'name' => $name,

	    		'date_from' => $date_from,

	    		'date_to' => $date_to,

	    		'time' => $time,

	    		'slug' => $slug,

	    		'location' => $location,

	    		'map_location' => $map_location,

	    		'price' => $price

	    	]);
    	}
    	
    }

    public function update_event($name, $date_from, $date_to, $time, $slug, $location, $map_location, $price, $id)
    {
    	$event = $this::find($id);

    	$event->name = $name;
    	
    	$event->date_from = $date_from;
    	
    	$event->date_to = $date_to;
    	
    	$event->time = $time;
    	
    	$event->slug = $slug;
    	
    	$event->location = $location;
    	
    	$event->map_location = $map_location;
    	
    	$event->price = $price;
    	
    	$event->save();
    }

    public function update_event_image($image_request, $id)
    {
    	$event = $this::find($id);

    	$image = Str::random(30).$image_request->getClientOriginalExtension();

    	if(!is_null($event->image))
    	{
    		Storage::delete('public/uploads/event/'.$event->image);
    	}

    	Storage::putFileAs('public/uploads/event', $image_request, $image);

    	$event->image = $image;

    	$event->save();
    }

    public function destroy_event($id)
    {
    	$event = $this::find($id);

    	if(!is_null($event->image))
    	{
    		Storage::delete('public/uploads/event/'.$event->image);
    	}

    	$event->delete();
    }

}
