<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Storage;

class CompanySettings extends Model
{
    protected $fillable = ['location', 'company_name', 'google_map_location', 'phone_number', 'email', 'facebook', 'twitter', 'google_plus', 'linked_in'];

     public function update_settings($location, $company_name, $google_map_location, $phone_number, $email, $logo, $favicon, $facebook, $twitter, $google_plus, $linked_in)
    {

    	$settings = $this::first();

    	/*
        /------------------------------------------------------------
        / If logo and favicon request is not null get its extension,
        / and Delete the existing file then update the new file.
        / while saving, move the new image in the public folder.
        /------------------------------------------------------------
        */

        /*
        /--------------------------------------------------------------------------
        / even If logo and favicon request is null just update the rest
        /--------------------------------------------------------------------------
        */

    	if (!is_null($logo) && !is_null($favicon)) 
    	{
    		$logo_update = 'logo.'.$logo->getClientOriginalExtension();
    		
    		$favicon_update = 'favicon.'.$favicon->getClientOriginalExtension();

    		Storage::delete(['public/uploads/'.$logo_update, 'public/uploads/'.$favicon_update]);

			Storage::putFileAs('public/uploads', $logo, $logo_update);

			Storage::putFileAs('public/uploads', $favicon, $favicon_update);

    	}

    	elseif(!is_null($logo) && is_null($favicon))

    	{

    		$logo_update = 'logo.'.$logo->getClientOriginalExtension();
    		
    		Storage::delete('public/uploads/'.$logo_update);

			Storage::putFileAs('public/uploads', $logo, $logo_update);

    	}

    	elseif(is_null($logo) && !is_null($favicon))

    	{

    		$favicon_update = 'favicon.'.$favicon->getClientOriginalExtension();

    		Storage::delete('public/uploads/'.$favicon_update);

			Storage::putFileAs('public/uploads', $favicon, $favicon_update);

    	}

    	$settings->location = $location;

    	$settings->google_map_location = $google_map_location;

    	$settings->email = $email;

    	$settings->company_name = $company_name;

    	$settings->phone_number = $phone_number;

        $settings->facebook = $facebook;

        $settings->google_plus = $google_plus;

        $settings->linked_in = $linked_in;

        $settings->twitter = $twitter;

    	$settings->save();

    }

}
