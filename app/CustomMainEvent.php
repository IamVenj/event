<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Storage;

class CustomMainEvent extends Model
{
    protected $fillable = ['event_id'];

    public function update_main_event($event_id, $main_event_image_request)
    {
    	$main_event = $this::first();

    	if(!is_null($main_event_image_request))
    	{
    		$main_image = 'main-event-img.'.$main_event_image_request->getClientOriginalExtension();

    		Storage::delete('public/uploads/custom-pages/custom-main-event/'.$main_image);

    		Storage::putFileAs('public/uploads/custom-pages/custom-main-event', $main_event_image_request, $main_image);
    	}

    	$main_event->event_id = $event_id;

    	$main_event->save();
    }
}
