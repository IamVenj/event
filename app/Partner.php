<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Str;

use Storage;

class Partner extends Model
{
    protected $fillable = ['image'];

    function create_partner($image_req)
    {
    	$image = Str::random(30).'.'.$image_req->getClientOriginalExtension();

    	$this::create([

    		'image' => $image

    	]);

    	Storage::putFileAs('public/uploads/partners', $image_req, $image);
    }

    function update_partner($image_req, $id)
    {
    	$partner = $this::find($id);

    	Storage::delete('public/uploads/partners/'.$partner->image);

    	$image = Str::random(30).'.'.$image_req->getClientOriginalExtension();

    	$partner->image = $image;

    	$partner->save();

    	Storage::putFileAs('public/uploads/partners', $image_req, $image);
    }

    function delete_partner($id)
    {
    	$partner = $this::find($id);

    	Storage::delete('public/uploads/partners/'.$partner->image);

    	$partner->delete();
    }
}
