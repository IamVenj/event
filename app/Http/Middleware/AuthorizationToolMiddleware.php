<?php

namespace App\Http\Middleware;

use Closure;

class AuthorizationToolMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd($request->has('event_id'));
            // dd($request->get('token'));

        if ($request->has('token')) {

         $request->headers->set('Authorization', 'Bearer ' . $request->get('token'));
        
        }

        return $next($request);
    }
}
