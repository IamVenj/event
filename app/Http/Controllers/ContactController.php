<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CompanySettings;

use App\Contact;

class ContactController extends Controller
{
    private $_contact, $_setting;

	public function __construct()
	{
		$this->_contact = new Contact();

		$this->_setting = new CompanySettings();
	}

    public function index()
    {
    	$setting = $this->_setting::first();

    	return view('pre-login.pages.contact', compact('setting'));
    }

    public function store()
    {
    	$this->validate(request(), [

    		'name' => 'required',

    		'email' => 'required|email',

    		'subject' => 'required',

    		'message' => 'required'

    	]);

    	$name = request('name');
    	$email = request('email');
    	$subject = request('subject');
    	$message = request('message');

    	$this->_contact->create_contact($name, $email, $subject, $message);

    	return back()->with('success', 'Successfully Sent. Thank You!');

    }

}
