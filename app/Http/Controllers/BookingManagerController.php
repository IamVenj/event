<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\UserEvent;

class BookingManagerController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

    public function index()
    {
    	if(auth()->user()->role_id == 2)
        {
            $user_event = UserEvent::where('user_id', auth()->user()->id)->get();

    		return view('pre-login/pages/user/booking', compact('user_event'));
        }
        else
        {
            return redirect('/dashboard');
        }
    }
}
