<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Password;

use App\User;

class ForgotPasswordController extends Controller
{
	use SendsPasswordResetEmails;

	private $_user;
    
    public function __construct()
    {
        $this->middleware('guest');

        $this->_user = new User();
    }

    public function index()
    {
    	return view('pre-login.auth.forgot-password');
    }

    public function sendResetLinkEmail(Request $request)
    {

        $this->validateEmail($request);

        if($this->_user->isConnected() == true)
        {

            $response = $this->broker()->sendResetLink(

                $request->only('email')

            );

            return $response == Password::RESET_LINK_SENT

                        ? $this->sendResetLinkResponse($request, $response)

                        : $this->sendResetLinkFailedResponse($request, $response);

        }
        else
        {

            return redirect()->back()->withErrors('Please Check Your Internet Connection!');

        }

    }
    protected function validateEmail(Request $request)
    {

        $request->validate(['email' => 'required|email']);

    }


    protected function sendResetLinkResponse(Request $request, $response)
    {

        return back()->with('success', trans($response));

    }


    protected function sendResetLinkFailedResponse(Request $request, $response)
    {

        return back()

                ->withInput($request->only('email'))

                ->withErrors(['email' => trans($response)]);

    }


    public function broker()
    {

        return Password::broker();

    }
}
