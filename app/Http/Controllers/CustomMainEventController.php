<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CustomMainEvent;

use App\Event;

class CustomMainEventController extends Controller
{
	private $_main_event, $_event;

	public function __construct()
	{
		$this->middleware('auth');

        $this->_main_event = new CustomMainEvent();

		$this->_event = new Event();
	}

    public function index()
    {
        if(auth()->user()->role_id == 1)
        {
            $events = $this->_event::all();

            $main_event = $this->_main_event::first();

    	   return view('post-login.pages.custom-pages.custom-main.create', compact('events', 'main_event'));
        }
        else
        {
            return redirect('/profile');
        }
    }

    public function update()
    {
    	$this::validate(request(),[

    		'event' => 'required',

    	]);


    	$event_img = request('image');

    	$event_id = request('event');
    	
        if(!is_null($event_img))
        {
            $this::validate(request(),[

                'image' => 'required|mimes:png,jpeg|max:5000'

            ]);
        }
    	
        $this->_main_event->update_main_event($event_id, $event_img);

    	return back()->with('success', 'Main Event is successfully updated!');

    }
}
