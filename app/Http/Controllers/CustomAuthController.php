<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Storage;

class CustomAuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if(auth()->user()->role_id == 1)
        {
    	   return view('post-login.pages.custom-pages.custom-auth.update');
        }
        else
        {
            return redirect('/profile');
        }
    }

    public function store()
    {
    	$this->validate(request(), [

    		'image' => 'required|mimes:png,jpeg|max:5000'

    	]);

    	$image = request('image');

    	$image_request = 'auth-img.'.$image->getClientOriginalExtension();

    	Storage::delete('public/uploads/custom-pages/custom-auth/'.$image_request);

    	Storage::putFileAs('public/uploads/custom-pages/custom-auth', $image, $image_request);

    	return back()->with('success', 'Auth Image is successfully updated!');
    }
}
