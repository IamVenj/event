<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Storage;

class CustomHomeController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

    public function index()
    {
        if(auth()->user()->role_id == 1)
        {
    	   return view('post-login.pages.custom-pages.custom-home.create');
        }
        else
        {
            return redirect('/profile');
        }
    }

    public function store()
    {
    	$this->validate(request(), [

    		'image' => 'required|mimes:jpeg|max:5000'

    	]);

    	$image = request('image');

    	$image_request = 'main-img.'.$image->getClientOriginalExtension();

    	Storage::delete('public/uploads/custom-pages/custom-main/'.$image_request);

    	Storage::putFileAs('public/uploads/custom-pages/custom-main', $image, $image_request);

    	return back()->with('success', 'main Image is successfully updated!');
    }
}
