<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Gallery;

class GalleryController extends Controller
{

	private $_gallery;

	public function __construct()
	{
		$this->_gallery = new Gallery();
	}

    public function index()
    {
    	$gallery = $this->_gallery::orderBy('created_at', 'desc')->paginate(16);

    	return view('pre-login.pages.gallery', compact('gallery'));
    }
}
