<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Event;

use App\UserEvent;

use App\User;

use App\EventCategory;

class EventController extends Controller
{

	private $_event, $_userEvent, $_category;

	public function __construct()
	{
		$this->_event = new Event();

        $this->_userEvent = new UserEvent();

        $this->_user = new User();

		$this->_category = new EventCategory();
	}

    public function index($category_name, $id)
    {

    	return view('pre-login.pages.events', compact('id', 'category_name'));
    }
    
    public function show($id)
    {

    	$event = $this->_event::find($id);

    	$_event_next = $this->_event::where('id', '>', $id)->get()->take(1);

		$_event_prev = $this->_event::where('id', '<', $id)->get()->take(1);

		$event_user = $this->_userEvent::where('event_id', $id)->get();

        return view('pre-login.pages.event', compact('event', '_event_prev', '_event_next', 'event_user')); 

    }

    public function register($event_id)
    {

    	$user_count = $this->_userEvent::where('user_id', auth()->user()->id)->where('event_id', $event_id)->count();

    	if($user_count == 0)
    	{

	    	$this->_userEvent->register_event(auth()->user()->id, $event_id);

	    	return back()->with('success', 'You have successfully registered on this event!');

	    }
    	else
    	{
    		return back()->withErrors('You are already registered on this event!');
    	}

    }
    
    public function cancel()
    {
        $_cancelling_user_id = auth()->user()->id;
        
        $event_id = request('event_id');
        
        $event = UserEvent::where('user_id', $_cancelling_user_id)->where('event_id', $event_id)->first();
        
        $event->delete();
        
        return back()->with('success', 'Your registration is successfully cancelled!');
        
    }

    

}
