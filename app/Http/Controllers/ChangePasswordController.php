<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use Hash;

class ChangePasswordController extends Controller
{
    private $_user;

	public function __construct()
	{
		$this->middleware('auth');

        $this->_user = new User();
	}

    public function index()
    {
    	if(auth()->user()->role_id == 2)
        {
    		return view('pre-login.auth.change-password');
        }
        else
        {
            return redirect('/dashboard');
        }
    }

    public function store()
    {
        $this::validate(request(), [

            'current_password' => 'required',

            'new_password' => 'required|min:5'

        ]);

        $user = $this->_user::find(auth()->user()->id);

        $current_password = request('current_password');

        $new_password = request('new_password');

        if(Hash::check($current_password, $user->password) == true)
        {
            $user->password = $new_password;

            $user->save();

            return back()->with('success', 'You have successfully changed your password');
        }

        else
        {
            return back()->withErrors('Your current password does not match!');
        }


    }
}
