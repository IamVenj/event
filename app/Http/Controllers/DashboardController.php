<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Contact;

use App\Service;

use App\Gallery;

use App\Event;

class DashboardController extends Controller
{
    private $_contact, $_service, $_gallery, $_event;

    public function __construct()
    {
        $this->middleware('auth');

        $this->_contact = new Contact();

        $this->_service = new Service();
        
        $this->_gallery = new Gallery();
        
        $this->_event = new Event();
    }

    public function index()
    {
    	if(auth()->user()->role_id == 1)
        {
            $contact = $this->_contact::all()->count();

            $service = $this->_service::all()->count();

            $gallery = $this->_gallery::all()->count();
            
            $event = $this->_event::all()->count();

    		return view('post-login.pages.dashboard', compact('contact', 'service', 'gallery', 'event'));
        }
        else
        {
            return redirect('/profile');
        }
    }
}
