<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Gallery;

class AdminGalleryController extends Controller
{
    private $_gallery;

	public function __construct()
	{
		$this->middleware('auth');

        $this->_gallery = new Gallery();
	}

    public function index()
    {
    	if(auth()->user()->role_id == 1)
        {
            $files = $this->_gallery::orderBy('created_at', 'desc')->paginate(6);

    		return view('post-login.pages.gallery.index', compact('files'));
        }
        else
        {
            return redirect('/profile');
        }
    }

    public function create()
    {
    	if(auth()->user()->role_id == 1)
        {
    		return view('post-login.pages.gallery.create');
        }
        else
        {
            return redirect('/profile');
        }
    }

    public function store()
    {
        $image = request('image');

        $name = request('name');

        $location = request('location');

        $this->validate(request(), [

            'name' => 'required',

            'location' => 'required',

            'image' => 'required|mimes:jpeg,png|max:5000'

        ]);


        $this->_gallery->add_file($name, $image, $location);

        return redirect('/admin-gallery')->with('success', 'Image is successfully uploaded!');

    }

    public function update($id)
    {
        $location = request('location');

        $name = request('name');

        $this->validate(request(), ['name' => 'required', 'location' => 'required']);

        $this->_gallery->update_file($name, $location, $id);

        return back()->with('success', 'File is successfully updated');
    }

    public function update_upload($id)
    {
        $image = request('image');

        $this->validate(request(), ['image' => 'required|mimes:jpeg,png|max:5000']);

        $this->_gallery->update_image($image, $id);

        return back()->with('success', 'Image is successfully updated');
    }

    public function destroy($id)
    {
        $this->_gallery->delete_file($id);

        return back()->with('success', 'File is successfully deleted!');
    }

}
