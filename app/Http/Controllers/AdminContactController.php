<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Contact;

class AdminContactController extends Controller
{

	private $_contact;

    public function __construct()
    {
        $this->_contact = new Contact();

        $this->middleware('auth');
    }

    public function index()
    {
    	if(auth()->user()->role_id == 1)
    	{
            $contacts = $this->_contact::orderBy('created_at', 'desc')->get();

    		return view('post-login.pages.contact.index', compact('contacts'));
    	}
    	else
    	{
    		return redirect('/profile');
    	}
    }


}
