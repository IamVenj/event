<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

class ProfileController extends Controller
{
	private $_user;

	public function __construct()
	{
		$this->middleware('auth');

		$this->_user = new User();

	}

    public function index()
    {
    	if(auth()->user()->role_id == 2)
    	{
	    	$user = $this->_user::find(auth()->user()->id);

	    	return view('pre-login.pages.user.profile', compact('user'));
    	}
    	else
    	{
    		return redirect('/dashboard');
    	}
    }

    public function update($id)
    {
    	$this::validate(request(), [

    		'email' => 'required|email',

    		'name' => 'required',

    		'gender' => 'required',

    		'phone_number' => 'required'

    	]);

    	$profile_picture = request('profile_picture');

    	$email = request('email');

    	$name = request('name');

    	$gender = request('gender');

    	$phone_number = request('phone_number');

    	if(!is_null($profile_picture))
    	{
    		$this::validate(request(), [

    			'profile_picture' => 'required|mimes:png,jpeg|max:5000'

    		]);
    	}

    	$this->_user->update_profile($name, $email, $gender, $profile_picture, $phone_number, $id);

    	return back()->with('success', 'Profile is successfully updated!');

    }


}
