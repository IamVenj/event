<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use App\Event;

use App\Gallery;

use App\UserEvent;

use App\Http\Resources\Event as EventResources;

use App\Http\Resources\Gallery as GalleryResources;

class ApiController extends Controller
{
    private $_user, $_userEvent, $_token;

    /*
    |---------------------------------------------------------------------
    | Constructor for ApiController Class
    |---------------------------------------------------------------------
    */
    
    public function __construct()
    {
        $this->_user = new User();

        $this->_userEvent = new UserEvent();

        $this->_token = null;        

    }

    /*
    |---------------------------------------------------------------------
    | A user registers & gets his/her access token
    |---------------------------------------------------------------------
    */
    
    public function register(Request $request)
    {
        $this::validate(request(),[

            'full_name' => 'required',

            'email' => 'required|email|unique:users',

            'phone_number' => 'required',

            'gender' => 'required',

            'password' => 'required|confirmed|min:4',

        ]);

        $name = request('full_name');

        $password = bcrypt(request('password'));

        $email = request('email');

        $mobile_number = request('phone_number');

        $gender = request('gender');
        
        $role = 2;
 
        $created_user = User::create([

            'name' => $name,

            'email' => $email,

            'phone_number' => $mobile_number,

            'password' => $password,

            'role_id' => $role,

            'gender' => $gender

        ]);

        $token = $created_user->createToken('ThisIsTheTokenIwantedToCreate')->accessToken;
 
        return response()->json(["token" => $token], 200);
        
    }

    /*
    |---------------------------------------------------------------------
    | A user logs in & gets his/her access token
    |---------------------------------------------------------------------
    */

    public function login(Request $request)
    {
    	$credentials = ['email' => $request->email, "password" => $request->password];

    	if(auth()->attempt($credentials))
    	{
            $this->_token = auth()->user()->createToken('ThisIsTheTokenIwantedToCreate')->accessToken;

            $request->headers->set('authorization', 'Bearer ' . $this->_token);

        	return response()->json(["token" => $this->_token], 200);
    	}
    	else
    	{
    		return response()->json(["error" => "unauthorized"], 401);
    	}
    }


    /*
    |------------------------------------------------------------------------
    | Get all event --> no paginations --> request for all data
    |------------------------------------------------------------------------
    */

    public function all_events()
    {
        $event = Event::all();
        
        return EventResources::collection($event);
    }

    /*
    |---------------------------------------------------------------------
    | A user registers on an event
    |---------------------------------------------------------------------
    */

    public function register_event($event_id, $token)
    {
        $request->headers->set('Authorization', 'Bearer ' . $token);

    	$user_count = $this->_userEvent::where('user_id', auth()->user()->id)->where('event_id', $event_id)->count();


    	if($user_count == 0)
    	{

	    	$this->_userEvent->register_event(auth()->user()->id, $event_id);

	    	return response()->json(['status'=>'success','data'=>'You are successfully registered on this event!'], 200);

	    }
    	else
    	{
    		return response()->json(['status'=>'success', 'data'=>'You are already registered on this event!'], 200);
    	}
    }

    /*
    |---------------------------------------------------------------------
    | A user cancels his/her registered event
    |---------------------------------------------------------------------
    */

    public function cancel_event_registration($event_id)
    {
        $_cancelling_user_id = auth()->user()->id;
               
        $event = $this->_userEvent::where('user_id', $_cancelling_user_id)->where('event_id', $event_id)->first();
        
        $event->delete();
        
        return response()->json(['status'=>'success', 'data'=>'Your registration is successfully cancelled!'], 200);
    }
    
	/*
    |---------------------------------------------------------------------
    | Get registered events
    |---------------------------------------------------------------------
    */

    public function get_registered_packages()
    {
    	$user_event = UserEvent::where('user_id', auth()->user()->id)->get();

    	foreach ($user_event as $combo) {
    		
    		$event = Event::where('id', $combo->event_id)->get();

    	}

    	return response()->json(["data" => $event], 200);
    }


    /*
    |--------------------------------------------------------------------------
    | Update Profile
    |--------------------------------------------------------------------------
    */

    public function update_profile()
    {

        $this::validate(request(), [

            'email' => 'required|email',

            'name' => 'required',

            'gender' => 'required',

            'phone_number' => 'required'

        ]);

        $id = auth()->user()->id;

        $profile_picture = request('profile_picture');

        $email = request('email');

        $name = request('name');

        $gender = request('gender');

        $phone_number = request('phone_number');

        if(!is_null($profile_picture))
        {
            $this::validate(request(), [

                'profile_picture' => 'required|mimes:png,jpeg|max:5000'

            ]);
        }

        $profile = $this->_user::find($id);

        if(!is_null($profile_picture))
        {

            $image = Str::random(30).'.'.$profile_picture->getClientOriginalExtension();
            
            if(!is_null($profile->profile_picture))
            {
                Storage::delete('public/uploads/user/profile/'.$profile->profile_picture);
            }

            Storage::putFileAs('public/uploads/user/profile', $profile_picture, $image);

            $profile->profile_picture = $image;
            
        }

        $profile->name = $name;

        $profile->gender = $gender;

        $profile->phone_number = $phone_number;

        $profile->save();

        return response()->json(['success' => 'Profile is successfully updated!', 'user' => $profile]);        

    }


    /*
    |--------------------------------------------------------------------------
    | Gallery
    |--------------------------------------------------------------------------
    */


    public function gallery()
    {
        $gallery = Gallery::all();
        
        return GalleryResources::collection($gallery);
    }


}
