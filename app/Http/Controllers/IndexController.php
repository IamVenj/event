<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\EventCategory;

use App\Event;

use App\Service;

use App\Partner;

use App\CustomMainEvent;

use App\CompanySettings;

class IndexController extends Controller
{

	private $_event, $_category, $_service, $_partner, $_setting;

	public function __construct()
	{
		$this->_event =  new Event();

		$this->_category = new EventCategory();

		$this->_service = new Service();

		$this->_partner = new Partner();

		$this->_setting = new CompanySettings();

		$this->_main_event = new CustomMainEvent();
	}

    public function index()
    {
    	$events = $this->_event::orderBy('created_at', 'desc')->get();

    	$services = $this->_service::orderBy('created_at', 'desc')->get()->take(4);

    	$_category_ = $this->_category::orderBy('created_at', 'desc')->get();

    	$partners = $this->_partner::orderBy('created_at', 'desc')->get();

    	$setting = $this->_setting::first();

    	$main_event = $this->_main_event::first();

    	return view('pre-login.pages.index', compact('events', '_category_', 'services', 'partners', 'setting', 'main_event'));
    }
}
