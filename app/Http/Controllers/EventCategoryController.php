<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\EventCategory;

class EventCategoryController extends Controller
{

	private $_category;

	public function __construct()
	{

		$this->_category = new EventCategory();

		$this->middleware('auth');
	}

    public function index()
    {
        if(auth()->user()->role_id == 1)
        {
        	$category = $this->_category::orderBy('created_at', 'desc')->paginate(8);

        	return view('post-login.pages.event-category.index', compact('category'));
        }
        else
        {
            return redirect('/profile');
        }
    }

    public function create()
    {
        if(auth()->user()->role_id == 1)
        {
    	   return view('post-login.pages.event-category.create');
        }
        else
        {
            return redirect('/profile');
        }
    }

    public function store()
    {
    	$this::validate(request(), [

    		'name' => 'required',

    	]);

    	$image = request('image');

    	$name = request('name');
 
    	if(!is_null($image))
    	{
    		$this::validate(request(), ['image' => 'required|mimes:png,jpeg|max:5000']);
    	}

    	$this->_category->create_category($name, $image);

    	return redirect('/admin-event-category')->with('success', 'Event category is successfully created!');

    }

    public function update($id)
    {

    	$this::validate(request(), ['name' => 'required']);

    	$name = request('name');

    	$this->_category->update_category($name, $id);

    	return back()->with('success', 'Event category is successfully updated!');

    }

    public function update_image($id)
    {
    	$this::validate(request(), ['image' => 'required|mimes:png,jpeg|max:5000']);

    	$image = request('image');

    	$this->_category->update_category_image($image, $id);

    	return back()->with('success', 'Event category image is successfully updated');

    }

    public function destroy($id)
    {
    	$this->_category->destroy_category($id);

    	return back()->with('success', 'Event category is successfully deleted!');
    }

}
