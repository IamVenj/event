<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Partner;

class AdminPartnerController extends Controller
{
    private $_partner;

    public function __construct()
    {
        $this->_partner = new Partner();

        $this->middleware('auth');
    }

    public function index()
    {
    	if(auth()->user()->role_id == 1)
        {
            $partners = $this->_partner::orderBy('created_at', 'desc')->get();

    		return view('post-login.pages.partners.index', compact('partners'));
        }
        else
        {
            return redirect('/profile');
        }
    }

    public function store()
    {
        $this->validate(request(), ['image' => 'required|mimes:png|max:5000']);

        $this->_partner->create_partner(request('image'));

        return back()->with('success', 'Partner is successfully created!');
    }

    public function destroy($id)
    {
        $this->_partner->delete_partner($id);

        return back()->with('success', 'Partner is successfully deleted!');
    }

    public function update($id)
    {
        $this->validate(request(), ['image' => 'required|mimes:png|max:5000']);

        $this->_partner->update_partner(request('image'), $id);

        return back()->with('success', 'Partner is successfully updated!');
    }
}
