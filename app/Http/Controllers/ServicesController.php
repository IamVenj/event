<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Service;

class ServicesController extends Controller
{
	private $_service;

    public function __construct()
    {
        $this->_service = new Service();
    }

    public function index()
    {
    	$services = $this->_service::orderBy('created_at', 'desc')->get();

    	return view('pre-login.pages.services', compact('services'));	
    }
}
