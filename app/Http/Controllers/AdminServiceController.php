<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Service;

class AdminServiceController extends Controller
{
    private $service;

    public function __construct()
    {
        $this->service = new Service();

        $this->middleware('auth');
    }

    public function index()
    {
    	if(auth()->user()->role_id == 1)
        {
            $services = $this->service::orderBy('created_at', 'desc')->paginate(9);

    		return view('post-login.pages.service.index', compact('services'));
        }
        else
        {
            return redirect('/profile');
        }
    }

    public function create()
    {
    	if(auth()->user()->role_id == 1)
        {
    		return view('post-login.pages.service.create');
        }
        else
        {
            return redirect('/profile');
        }
    }

    public function store()
    {
        $icon = request('icon');

        $title = request('title');
        
        $slug = request('slug');

        $this->validate(request(),[

            'title' => 'required',

            'slug' => 'required'

        ]);

        $this->service->create_service($icon, $title, $slug);

        return redirect('admin-service')->with('success', 'Service is successfully created');

    }

    public function update($id)
    {

        $icon = request('icon');

        $title = request('title');
        
        $slug = request('slug');

        $this->validate(request(), [

            'icon' => 'required',

            'title' => 'required',

            'slug' => 'required'

        ]);

        $this->service->update_service($icon, $title, $slug, $id);

        return back()->with('success', 'Service is successfully updated!');
    }

    public function destroy($id)
    {
        $this->service->delete_service($id);

        return back()->with('success', 'Service is successfully Deleted!');
    }
}
