<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    private $_user;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');

        $this->_user = new User();
    }

    public function index()
    {
        return view('pre-login.auth.register');
    }

    public function store()
    {
        $this::validate(request(),[

            'full_name' => 'required',

            'email' => 'required|email|unique:users',

            'phone_number' => 'required',

            'gender' => 'required',

            'password' => 'required|confirmed|min:4'

        ]);

        $name = request('full_name');

        $password = bcrypt(request('password'));

        $email = request('email');

        $mobile_number = request('phone_number');

        $gender = request('gender');

        $role = 2;
 
        $this->_user->create_user($name, $email, $mobile_number, $password, $role, $gender);

        if(auth()->attempt(request(['email', 'password'])))
        {
            if(auth()->user()->role_id == 1)
            {
                return redirect('/dashboard')->with('success', 'You are successfully registered');
            }
            else
            {
                return redirect('/profile')->with('success', 'You are successfully registered');
            }
        }

    }
}
