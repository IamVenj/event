<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Event;

use App\EventCategory;

use App\UserEvent;

use Carbon\Carbon;

class AdminEventController extends Controller
{
    private $_event, $_category;

    public function __construct()
    {
        $this->_event = new Event();
     
        $this->_category = new EventCategory();

        $this->_userEvent = new UserEvent();

        $this->middleware('auth');
    }

    public function index()
    {

        if(auth()->user()->role_id == 1)
        {
            $events = $this->_event::orderBy('created_at', 'desc')->paginate(8);

            $_category_ = $this->_category::all();

        	return view('post-login.pages.event.index', compact('events', '_category_'));

        }
        else
        {
            return redirect('/profile');
        }

    }

    public function create()
    {
        if(auth()->user()->role_id == 1)
        {
            $category = $this->_category::all();

        	return view('post-login.pages.event.create', compact('category'));

        }
        else
        {
            return redirect('/profile');
        }
    }

    public function showEventRegisters($id)
    {
        if(auth()->user()->role_id == 1)
        {
            $_user_event_ = $this->_userEvent::where('event_id', $id)->get();

    	    return view('post-login.pages.event.event-registered-personnel', compact('_user_event_'));
        }
        else
        {
            return redirect('/profile');
        }
    }

    public function store()
    {
        $this::validate(request(), [

            'name' => 'required',

            'date_from' => 'required',

            'date_to' => 'required',

            'time' => 'required',

            'slug' => 'required',

            'location' => 'required',

            'price' => 'required'

        ]);

        $name = request('name');

        $date_from = Carbon::createFromFormat('d/m/Y', request('date_from'))->format('Y-m-d');

        $date_to = Carbon::createFromFormat('d/m/Y', request('date_to'))->format('Y-m-d');

        $time = request('time');

        $image_request = request('image');

        $slug = request('slug');

        $location = request('location');

        $map_location = request('map_location');

        $price = request('price');

        if(!is_null(request('image')))
        {
            $this::validate(request(), ['image' => 'required|mimes:png,jpeg|max:5000']);
        }

        $this->_event->create_event($name, $date_from, $date_to, $time, $image_request, $slug, $location, $map_location, $price);

        return redirect('/event-admin')->with('success', 'Event is successfully created');

    }


    public function update($id)
    {
        $this::validate(request(), [

            'name' => 'required',

            'date_from' => 'required',

            'date_to' => 'required',

            'time' => 'required',

            'slug' => 'required',

            'location' => 'required',

            'price' => 'required'

        ]);

        $name = request('name');

        $date_from = Carbon::createFromFormat('d/m/Y', request('date_from'))->format('Y-m-d');

        $date_to = Carbon::createFromFormat('d/m/Y', request('date_to'))->format('Y-m-d');

        $time = request('time');

        $slug = request('slug');

        $location = request('location');

        $map_location = request('map_location');

        $price = request('price');

        $this->_event->update_event($name, $date_from, $date_to, $time, $slug, $location, $map_location, $price, $id);

        return back()->with('success', 'Event is successfully updated');
    }

    public function update_image($id)
    {
        $this::validate(request(), ['image' => 'required|mimes:png,jpeg|max:5000']);

        $image = request('image');

        $this->_event->update_event_image($image, $id);

        return back()->with('success', 'Event image is successfully updated');
    }

    public function destroy($id)
    {
        $this->_event->destroy_event($id);

        return back()->with('success', 'Event is successfully deleted!');
    }
}
