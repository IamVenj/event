<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/register', 'ApiController@register');

Route::post('/login', 'ApiController@login');


Route::get('/gallery', 'ApiController@gallery');

Route::get('/all-events', 'ApiController@all_events');

Route::middleware('auth:api')->group(function(){

	Route::post('/register-event/{event_id}', 'ApiController@register_event');

	Route::post('/update-profile', 'ApiController@update_profile');

	Route::delete('/cancel-registration/{event_id}', 'ApiController@cancel_event_registration');

	Route::get('/get-registered-events', 'ApiController@get_registered_packages');	
});
