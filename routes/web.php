<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Route::get('/contact', 'ContactController@index');
Route::post('/contact', 'ContactController@store');

Route::get('/event/{id}', 'EventController@show');

Route::get('/category/{category_name}/{id}', 'EventController@index');

Route::get('/services', 'ServicesController@index');
Route::get('/gallery', 'GalleryController@index');

/*
/---------------
/ Auth
/---------------
*/
Route::get('/login', 'Auth\LoginController@index')->name('login');
Route::post('/login', 'Auth\LoginController@store');
Route::get('/logout', 'Auth\LoginController@destroy');

Route::get('/register', 'Auth\RegisterController@index');
Route::post('/register', 'Auth\RegisterController@store');




Route::get('/dashboard', 'DashboardController@index');

Route::get('/admin-account', 'AdminAccountController@index');
Route::patch('/admin-account', 'AdminAccountController@store');

Route::get('/company-settings', 'CompanySettingsController@index');
Route::post('/company-settings', 'CompanySettingsController@store');


/*
/------------------------------------------------------------------------------------------------------
/ Custom Pages
/------------------------------------------------------------------------------------------------------
*/

Route::get('/main-custom', 'CustomHomeController@index');
Route::post('/main-custom', 'CustomHomeController@store');

Route::get('/custom-main-event', 'CustomMainEventController@index');
Route::post('/custom-main-event', 'CustomMainEventController@update');

Route::get('/custom-auth', 'CustomAuthController@index');
Route::post('/custom-auth', 'CustomAuthController@store');

Route::get('/partners', 'AdminPartnerController@index');
Route::post('/partners', 'AdminPartnerController@store');
Route::patch('/partners/{id}/image', 'AdminPartnerController@update');
Route::delete('/partners/{id}', 'AdminPartnerController@destroy');


Route::get('/admin-contact', 'AdminContactController@index');


Route::get('/admin-service', 'AdminServiceController@index');
Route::patch('/admin-service/{id}', 'AdminServiceController@update');
Route::delete('/admin-service/{id}', 'AdminServiceController@destroy');
Route::get('/add-service', 'AdminServiceController@create');
Route::post('/add-service', 'AdminServiceController@store');

Route::get('/admin-gallery', 'AdminGalleryController@index');
Route::patch('/admin-gallery/{id}', 'AdminGalleryController@update');
Route::patch('/admin-gallery/{id}/image', 'AdminGalleryController@update_upload');
Route::delete('/admin-gallery/{id}', 'AdminGalleryController@destroy');
Route::get('/add-image', 'AdminGalleryController@create');
Route::post('/add-image', 'AdminGalleryController@store');

Route::get('/admin-event-category', 'EventCategoryController@index');
Route::get('/add-event-category', 'EventCategoryController@create');
Route::post('/add-event-category', 'EventCategoryController@store');
Route::patch('/admin-event-category/{id}', 'EventCategoryController@update');
Route::patch('/admin-event-category/{id}/image', 'EventCategoryController@update_image');
Route::delete('/admin-event-category/{id}', 'EventCategoryController@destroy');

Route::get('/event-admin', 'AdminEventController@index');
Route::patch('/event-admin/{id}', 'AdminEventController@update');
Route::patch('/event-admin/{id}/image', 'AdminEventController@update_image');
Route::delete('/event-admin/{id}', 'AdminEventController@destroy');
Route::get('/add-event', 'AdminEventController@create');
Route::post('/add-event', 'AdminEventController@store');
Route::get('/register-list/{id}/event/list', 'AdminEventController@showEventRegisters');

Route::get('/forgot-password', 'ForgotPasswordController@index');

Route::patch('/register-event/{id}', 'EventController@register');
Route::patch('/cancel-registration', 'EventController@cancel');


Route::get('/profile', 'ProfileController@index');
Route::patch('/profile/{id}', 'ProfileController@update');
Route::get('/my-booking', 'BookingManagerController@index');

Route::get('/update-password', 'ChangePasswordController@index');
Route::patch('/update-password', 'ChangePasswordController@store');


/*
/------------------------
/ Forgot Password - Pre-login
/------------------------
*/
Route::get('/forgot-password', 'ForgotPasswordController@index');
Route::post('/forgot-password', 'ForgotPasswordController@sendResetLinkEmail');

Route::get('/reset-password/{token}', 'Auth\ResetPasswordController@showResetForm')->name('PasswordReset');
Route::post('/reset-password', 'Auth\ResetPasswordController@reset');


/*
|-----------------------------------------
| Setup needed when not having SSH access
|-----------------------------------------
*/




/*
|-----------------------------------------
| Cache Issues
|-----------------------------------------
*/

Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    Artisan::call('route:clear');
    Artisan::call('view:clear');
    return "Cache is cleared";
});

Route::get('/migrate', function() {
    Artisan::call('migrate');
    return "Migration is successful";
});

/*
|------------------------------------------
| Controllers
|------------------------------------------
*/

Route::get('/create-user-controller', function() {
    Artisan::call('make:controller UserController');
    return 'controller user is successfully created!';
});

Route::get('/create-event-controller', function() {
    Artisan::call('make:controller EventAPIController');
    return 'controller event is successfully created!';
});

/*
|------------------------------------------
| Resource
|------------------------------------------
*/

Route::get('/event-resource', function(){
   Artisan::call('make:resource Event');
   return 'Evetnt resource is successfully created!';
});
