<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        factory(App\User::class)->create([

            'name' => 'admin',

            'email' => 'admin@admin.com',

            'password' => bcrypt('eventadmin'),

            'role_id' => 1

        ]);


        DB::table('company_settings')->delete();

        factory(App\CompanySettings::class)->create([

            'company_name' => 'Event Company',

            'location' => '291 South 21th Street, Suite 721 New York NY 10016',

            'google_map_location' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d31520.976677117276!2d38.757750291144525!3d9.052627268559279!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x164b8f642ba9e021%3A0x2321687a606863f3!2sAddis+Ababa+Institute+of+Technology!5e0!3m2!1sen!2set!4v1555660366664!5m2!1sen!2set',
            
            'phone_number' => '+ 1235 2355 98',
            
            'email' => 'info@yoursite.com',

        ]);

        DB::table('custom_main_events')->delete();

        factory(App\CustomMainEvent::class)->create([

            'event_id' => null,

        ]);
    }
}
