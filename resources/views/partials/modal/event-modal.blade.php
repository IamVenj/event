<?php

$date_to = Carbon\Carbon::createFromFormat('Y-m-d', $event->date_to)->format('d/m/Y');

$date_from = Carbon\Carbon::createFromFormat('Y-m-d', $event->date_from)->format('d/m/Y');


?>

<div class="modal modal-edu-general Customwidth-popup-WarningModal fade" role="dialog" id=<?= 'edit-event'.$event->id;?>>

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header header-color-modal bg-color-3">

                <h4 class="modal-title"><i class="fa fa-edit"></i> Edit Event </h4>

                <div class="modal-close-area modal-close-df">

                    <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close" style="color: #fff;"></i></a>

                </div>

            </div>

            <form action="/event-admin/{{$event->id}}" method="post">
                
                @csrf

                @METHOD('PATCH')

                <div class="modal-body">

                    <div class="form-group">

                        <label for="name" style="font-size: 15px;">Event Name <small style="color: red;">*</small></label>

                        <input type="text" class="form-control" name="name" id="name" value="{{$event->name}}" placeholder="event name" style="font-size: 15px;">

                    </div>

                    <div class="form-group">

                        <label for="category" style="font-size: 15px;">Event Category <small style="color: red;">*</small></label>

                        <select class="form-control" style="font-size: 17px;" name="event_category_id">

                            <option value="" selected="" disabled="">Please select an event category</option>
                            
                            @foreach($_category_ as $event_category)
                                            
                            <option value="{{$event_category->id}}" <?php if($event->event_category_id == $event_category->id):?>selected<?php endif;?>>{{$event_category->name}}</option>
                            
                            @endforeach

                        </select>

                    </div>

                    <!-- <div class="row"> -->
                        
                        <!-- <div class="col-md-4">                                           -->

                            <div class="form-group">
                                <label for="dtp_input2" class="control-label">Date: from</label>
                                <div class="input-group date form_date" data-date="" data-date-format="d/m/yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                                    <input class="form-control" size="16" type="text" value="{{$date_from}}" name="date_from" readonly style="font-size: 16px;">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                    <!-- <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span> -->
                                </div>
                                <input type="hidden" id="dtp_input2" value="" /><br/>
                            </div>

                        <!-- </div> -->

                        <!-- <div class="col-md-4"> -->
                            
                            <div class="form-group">
                                <label for="dtp_input2" class="control-label">Date: to</label>
                                <div class="input-group date form_date" data-date="" data-date-format="d/m/yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                                    <input class="form-control" size="16" type="text" name="date_to" value="{{$date_to}}" readonly style="font-size: 16px;">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                    <!-- <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span> -->
                                </div>
                                <input type="hidden" id="dtp_input2" value="" /><br/>
                            </div>

                        <!-- </div> -->

                        <!-- <div class="col-md-4"> -->
                            
                            <div class="form-group">
                                <label for="dtp_input3">Select Time</label>
                                <div class="input-group date form_time" data-date="" data-date-format="hh:ii:ss" data-link-field="dtp_input3" data-link-format="hh:ii:ss">
                                    <input class="form-control" size="16" type="text" value="" name="time" readonly style="font-size: 16px;">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                                </div>
                                <input type="hidden" id="dtp_input3" value=""/><br/>
                            </div>

                        <!-- </div> -->

                    <!-- </div> -->

                    

                    <div class="form-group">

                        <label for="slug" style="font-size: 15px;">slug <small style="color: red;">*</small></label>

                        <textarea class="form-control"  rows="8" name="slug" id="slug" placeholder="Description/slug" style="font-size: 15px;" required><?= $event->slug; ?></textarea>

                    </div>

                    <div class="form-group">

                        <label for="location" style="font-size: 15px;">Location <small style="color: red;">*</small></label>

                        <input type="text" class="form-control" name="location" value="{{$event->location}}" id="location" placeholder="Location" style="font-size: 15px;">

                    </div>

                    <div class="form-group">

                        <label for="map_location" style="font-size: 15px;">Map Location <small style="color: red;">*</small></label>

                        <input type="text" class="form-control" name="map_location" value="{{$event->map_location}}" id="map_location" placeholder="Map Location" style="font-size: 15px;">

                    </div>

                    <div class="form-group">

                        <label for="price" style="font-size: 15px;">Price <small style="color: red;">*</small></label>

                        <input type="text" class="form-control" value="{{$event->price}}" name="price" id="price" placeholder="Price" style="font-size: 15px;">

                    </div>

                </div>

                <div class="modal-footer">

                    <button class="btn btn-secondary" style="padding: 10px; padding-left: 15px; padding-right: 15px;" data-dismiss="modal">Cancel</a>

                    <button type="submit" class="btn btn-primary" style="padding: 10px; padding-left: 15px; padding-right: 15px;">Save Changes</button>

                </div>

            </form>


        </div>

    </div>

</div>

<div class="modal modal-edu-general Customwidth-popup-WarningModal fade" role="dialog" id=<?= 'edit-event-image'.$event->id;?>>

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header header-color-modal bg-color-3">

                <h4 class="modal-title"><i class="fa fa-edit"></i> Edit Event Image</h4>

                <div class="modal-close-area modal-close-df">

                    <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close" style="color: #fff;"></i></a>

                </div>

            </div>

            <form action="/event-admin/{{$event->id}}/image" method="post" enctype="multipart/form-data">
                
                @csrf

                @METHOD('patch')                

                <div class="modal-body">

                   <div class="form-group">

                        <label for="image" style="font-size: 15px;">Image</label>

                        <input type="file" name="image" class="form-control" id="image" placeholder="Image" style="font-size: 15px; font-weight: bold;">

                    </div>

                    @if(!is_null($event->image))

                    <img src="{{URL::asset('storage/app/public/uploads/event/'.$event->image)}}" class="img-card" style="width: 470px; height: 400px; object-fit: cover;">

                    @endif

                </div>

                <div class="modal-footer">

                    <button class="btn btn-secondary" style="padding: 10px; padding-left: 15px; padding-right: 15px;" data-dismiss="modal">Cancel</a>

                    <button type="submit" class="btn btn-primary" style="padding: 10px; padding-left: 15px; padding-right: 15px;">Save Changes</button>

                </div>

            </form>


        </div>

    </div>

</div>

<div class="modal fade" role="dialog" style="border: none;" id=<?= 'delete-event'.$event->id;?>>

    <div class="modal-dialog">

        <div class="modal-content">

            <form action="/event-admin/{{$event->id}}" method="post">
                
                @csrf

                @method('DELETE')

                <div class="modal-header header-color-modal bg-color-4" style="background: rgb(230, 82, 81); color: #fff;">

                    <h4 class="modal-title" style="font-size: 17px;"><i class="fa fa-trash-o"></i> Delete Event</h4>

                    <div class="modal-close-area modal-close-df">

                        <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close" style="color: #fff;"></i></a>

                    </div>

                </div>


                <div class="modal-footer">

                    <button class="btn btn-secondary" style="padding: 10px; padding-left: 15px; padding-right: 15px;" data-dismiss="modal">Cancel</a>

                    <button type="submit" class="btn btn-danger" style="padding: 10px; padding-left: 15px; padding-right: 15px;">Yes, I want to Delete</button>

                </div>

            </form>

            

        </div>

    </div>

</div>
