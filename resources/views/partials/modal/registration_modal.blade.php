<div class="modal fade" role="dialog" style="border: none;" id=<?= 'register-event'?>>

    <div class="modal-dialog">

        <div class="modal-content">

            <form action="/register/event" method="post">
                
                @csrf

                <div class="modal-header" style="background: rgb(230, 82, 81); color: #fff;">

                    <h4 class="modal-title" style="font-size: 17px;"><i class="fa fa-trash-o"></i> Register for Event Name</h4>

                    <div class="modal-close-area modal-close-df">

                        <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close" style="color: #fff;"></i></a>

                    </div>

                </div>


                <div class="modal-footer">

                    <button class="btn btn-default" style="padding: 10px; padding-left: 15px; padding-right: 15px;" data-dismiss="modal">Cancel</a>

                    <button type="submit" class="btn btn-success" style="padding: 10px; padding-left: 15px; padding-right: 15px;">Yes, I want to Register</button>

                </div>

            </form>            

        </div>

    </div>

</div>