@extends('pre-login.index.index')

@section('content')


<div class="main-wrapper">
    <div class="main">
        <div class="main-inner">
            <div class="content" style="padding-bottom:  0px;">
                <div class="hero hero-creative-wrapper">
					<div class="hero-creative">
						<div class="hero-creative-image" style="background-image: url('{{URL::asset('storage/app/public/uploads/custom-pages/custom-auth/auth-img.jpg')}}');">
							<div class="hero-creative-title">
								<h2>Profile</h2>

								<form action="/profile/{{auth()->user()->id}}" method="post" class="hero-creative-search" style="margin-top: 0px;" enctype="multipart/form-data">
									
									@METHOD('patch')
									
									@csrf
									
									<fieldset style="border-bottom-left-radius: 50px; border-top-right-radius: 50px; padding-top: 30px; padding-bottom: 30px;">

										<div class="user-avatars center" style="padding-top: 0px;">

											@if(auth()->user()->profile_picture)

											<a href="{{URL::asset('storage/app/public/uploads/user/profile/'.auth()->user()->profile_picture)}}" data-background="image" data-src="{{URL::asset('storage/app/public/uploads/user/profile/'.auth()->user()->profile_picture)}}" data-lightbox="events" class="user-avatar" style="background-image: url('{{URL::asset('storage/app/public/uploads/user/profile/'.auth()->user()->profile_picture)}}');"></a>

											@else

											<p class="center">Choose Profile Picture <i class="fa fa-caret-down"></i></p>
											<div style=" border-radius: 100px; box-shadow: 1px 10px 20px rgba(35, 35, 35, 0.2); height: 50px;  background: linear-gradient(120deg, #00e4d0, #429FFD);" alt="Card image cap"></div>

											@endif


										</div><!-- /.user-avatars -->
										<div class="center" style="padding-top: 20px; padding-bottom: 20px;">
											
											<input type="file" name="profile_picture">
										</div>

										<div class="row">
										    <div class="col-sm-4">
										        <div class="form-group">
										            <label>E-mail</label>
										            <input type="email" name="email" placeholder="E-mail" value="{{auth()->user()->email}}" class="form-control " style="border-radius: 5px; box-shadow: 1px 10px 10px rgba(0,0,0,0.05); padding: 25px;">
										        </div><!-- /.form-group -->
										    </div><!-- /.col-* -->

										    <div class="col-sm-8">
										        <div class="form-group">
										            <label>Full Name</label>
										            <input type="text" class="form-control " name="name" value="{{auth()->user()->name}}" placeholder="full name" style="border-radius: 5px; box-shadow: 1px 10px 10px rgba(0,0,0,0.05); padding: 25px;">
										        </div><!-- /.form-group -->
										    </div><!-- /.col-* -->

										    <div class="col-sm-6">
										        <div class="form-group">
										            <label>Gender</label>
										           <select class="form-control form-control-lg" name="gender" style="border-radius: 5px; box-shadow: 1px 10px 10px rgba(0,0,0,0.05); ">
							                          <option selected disabled>Select Gender</option>
							                          <option value="m" <?php if(auth()->user()->gender == 'm'):?>selected<?php endif;?>>Male</option>
							                          <option value="f" <?php if(auth()->user()->gender == 'f'):?>selected<?php endif;?>>Female</option>
							                        </select>
										        </div><!-- /.form-group -->
										    </div><!-- /.col-* -->

										    <div class="col-sm-6">
										        <div class="form-group">
										            <label>Phone Number</label>
										            <input type="text" class="form-control " name="phone_number" value="{{auth()->user()->phone_number}}" placeholder="phone number" style="border-radius: 5px; box-shadow: 1px 10px 10px rgba(0,0,0,0.05); padding: 25px;">
										        </div><!-- /.form-group -->
										    </div><!-- /.col-* -->

										</div>
										<button class="btn btn-success" type="submit" style="margin-top: 30px; border-radius: 3px; box-shadow: 1px 5px 20px rgba(35, 35, 35, 0.2)"><i class="fa fa-upload" style="margin-right: 5px;"></i>Update</button>	
									</fieldset>

								</form>
							</div	><!-- /.hero-creative-title -->

													
						</div><!-- /.hero-image -->
					</div><!-- /.hero-creative -->
				</div><!-- /.hero -->
			</div>
		</div>
	</div>
</div>


@include('partials.error2')

@include('partials.success2')


@endsection