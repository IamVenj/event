@extends('pre-login.index.index')

@section('content')

<div class="container-fluid push-bottom">
	<div class="page-title">
		<h1 style="font-weight: bold;">My Bookings</h1>
	</div><!-- /.page-title -->

	<div class="row">

		@foreach($user_event as $event)

			<?php

			$events = App\Event::where('id', $event->event_id)->get();

			?>

			@foreach($events as $_event)

			<div class="col-lg-4 col-xl-4 col-6">
				<div class="card">
					<div class="card-inner">
						<div class="card-image">
							<a href="/event/{{$_event->id}}" style="background-image: url('{{URL::asset('storage/app/public/uploads/event/'.$_event->image)}}');">
								<span><i class="fa fa-search"></i></span>
							</a>

							<div class="card-actions">
								<a href="#"><i class="fa fa-user"></i> </a><p style="color: #fff;">{{App\UserEvent::where('event_id', $_event->id)->count()}}</p>
							</div><!-- /.card-actions -->
						</div><!-- /.card-image -->

						<div class="card-content">
						    <div class="row">
                                
                                <div class="col-md-6">	
							<div class="card-date">
								<strong>{{date('d', strtotime($_event->date_from))}}</strong>
								<span>{{date('M', strtotime($_event->date_from))}}</span>
							</div><!-- /.card-date -->

							<div class="card-date">
								<strong>{{date('d', strtotime($_event->date_to))}}</strong>
								<span>{{date('M', strtotime($_event->date_from))}}</span>
							</div>

                            
        							<h3 class="card-title">
        								<a href="/event/{{$_event->id}}">{{$_event->name}}</a>
        							</h3>
    							
    
        							<h4 class="card-subtitle">
        								<a href="/event/{{$_event->id}}">{{$_event->location}}</a>
        							</h4>
    							
    						    </div>
    							
    							<form action="/cancel-registration" method="post">
    							@csrf
    							@method('patch')
    							<div class="col-md-6">
    							    <input type="hidden" value="{{$_event->id}}" name="event_id"/>
    							    <button type="submit" class="btn btn-danger">Cancel Registration</button>
    							
    							</div
    							</form>
							</div>
						</div><!-- /.card-content -->
					</div><!-- /.card-inner -->
				</div><!-- /.card -->
			</div><!-- /.col-* -->

			@endforeach

		@endforeach

	</div><!-- /.row -->
	
	
</div><!-- /.container-fluid -->

@include('partials.error2')
@include('partials.success2')

@endsection