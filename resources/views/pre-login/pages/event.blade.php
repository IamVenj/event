@extends('pre-login.index.index')

@section('content')

<style type="text/css">
	.page-header.background-image {
    background-image: url('@if(!is_null($event->image)) {{URL::asset('storage/app/public/uploads/event/'.$event->image)}} @else {{URL::asset('storage/app/public/uploads/custom-pages/custom-main/main-img.jpg')}} @endif');
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat; }
</style>

<div class="main-wrapper">
    <div class="main">
        <div class="main-inner">
	        
            <div class="content">
            	<div class="page-header pull-top background-image">
					<div class="page-header-inner">
						<div class="container-fluid">
						</div><!-- /.container-fluid -->
					</div><!-- /.page-header-inner -->
				</div><!-- /.page-header -->

				@if($event_user->count() > 0)

               <div class="page-header pull-top">
					<div class="page-header-inner">
						<div class="container-fluid">
							<h1>Recently registered Users</h1>

							<div class="user-avatars">

								@foreach($event_user as $u)

									<?php

									$_users = App\User::where('id', $u->user_id)->get();

									?>

									@foreach($_users as $user)
									
										@if(!is_null($user->profile_picture))

											<a data-toggle="tooltip" data-background="image" data-src="{{URL::asset('storage/app/public/uploads/user/profile/'.$user->profile_picture)}}" href="{{URL::asset('storage/app/public/uploads/user/profile/'.$user->profile_picture)}}" data-lightbox="user" title="{{$user->name}}" class="user-avatar" style="background-image: url({{URL::asset('storage/app/public/uploads/user/profile/'.$user->profile_picture)}})"></a>

										@else

											<a data-toggle="tooltip" href="#" title="{{$user->name}}" class="user-avatar" style="background-image: url('{{URL::asset('storage/app/public/uploads/custom-pages/custom-main/main-img.jpg')}}"></a>

										@endif

									@endforeach	

								@endforeach	
							
							</div><!-- /.user-avatars -->
						</div><!-- /.container-fluid -->
					</div><!-- /.page-header-inner -->
				</div><!-- /.page-header -->

				@endif				

				<div class="container-fluid">
					<ol class="breadcrumb">
						<li><a href="/">Home</a></li>
						<li class="active">{{$event->name}}</li>
					</ol>
				</div><!-- /.container-fluid -->

				<div class="container-fluid">
					<div class="row">
						<div class="col-sm-8">
							<div class="information">
								<p>
								 	<?= $event->slug;?>
								</p>

								
							</div><!-- /.information -->


							

							<div class="page-navigation">
								
								@if(!is_null($_event_prev))
								
								@foreach($_event_prev as $prev)
								
								<a href="{{$prev->id}}" class="prev"><i class="fa fa-chevron-left"></i> {{$prev->name}} </a>
								
								@endforeach
								
								@endif
								
								@if(!is_null($_event_next))
								
								@foreach($_event_next as $next)
								
								<a href="{{$next->id}}" class="next"> {{$next->name}} <i class="fa fa-chevron-right"></i></a>
								
								@endforeach
								
								@endif
							</div><!-- /.page-navigation -->
						</div><!-- /.col-* -->

						<div class="col-sm-4 wd">
							
							<div class="widget purchase">
							
								<h3>{{$event->name}}</h3>
							
								<p>{{date('d M y', strtotime($event->date_from))}} - {{date('d M y', strtotime($event->date_to))}} | {{date('h:i', strtotime($event->time))}}</p>
							
								<p>{{$event->location}}</p>
							
								<strong>{{$event->price}}</strong>
							
								<div class="purchase-action">
							
									<a @guest  href="/login" @else id="register-event" @endguest class="btn btn-secondary pull-right">
							
										Register						
							
									</a>
							
								</div><!-- /.purchase-action -->
							
							</div><!-- /.purchase -->

							<div class="slide-register" id="register" style="display:none; background: #fff; border-radius: 10px; box-shadow: 1px 10px 20px rgba(35, 35, 35, 0.06)">
                
				                <div class="modal-header" style="background: #5cb85c; color: #fff;">

				                    <h4 class="modal-title" style="font-size: 17px; color: #fff"><i class="fa fa-check"></i> Register for Event Name</h4>

				                </div>


				                <div class="modal-footer">
				                	<div class="row">

				                		<div class="col-md-4" style="padding-top: 10px;">
				                			
				                    		<button class="btn btn-default" id="cancel-register">Cancel</button>
				                		
				                		</div>
				                		
				                		<div class="col-md-6" style="padding-top: 10px;">
				                			
						                	<form action="/register-event/{{$event->id}}" method="post">

						                		@METHOD('PATCH')

						                		@csrf
						                		
						                    	<button type="submit" class="btn btn-success">Yes, I want to Register</button>
						                	
						                	</form>
				                		
				                		</div>

				                	</div>



				                </div>

				            </div>

							@if(!is_null($event->map_location))

							<div class="widget map">
								<div class="map-inner">
									<!-- <div id="event-map"></div> -->
									
									<?php
									
									$path = parse_url($event->map_location, PHP_URL_PATH);
                                    $encoded_path = array_map('urlencode', explode('/', $path));
                                    $url = str_replace($path, implode('/', $encoded_path), $event->map_location);
									
									?>

                                    @if(filter_var($event->map_location, FILTER_VALIDATE_URL) == true)

									<iframe src="{{$event->map_location}}" frameborder="0" style="border:0; border-radius: 20px; height: 240px; box-shadow: 1px 10px 10px rgba(35, 35, 35, 0.09);" allowfullscreen></iframe>

                                    @else
                                    
                                    <p style="color: red;"> Please Provide a correct google map url. Go into google maps, select share then click embed a map. You will find the appropriate URL inside the src tag</p>
                                    
                                    @endif

									<div class="map-address">
										{{$event->location}}
									</div><!-- /.map-address -->
								</div><!-- /.map-inner -->
							</div><!-- /.map -->
							
							@endif
										
						</div><!-- /.col-* -->
					</div><!-- /.row -->
				</div><!-- /.container-fluid -->
            </div><!-- /.content -->
        </div><!-- /.main-inner -->
    </div><!-- /.main -->
</div><!-- /.main-wrapper -->
<script type="text/javascript" src="{{URL::asset('assets/js/jquery.js')}}"></script>
<script type="text/javascript">
	
	$(document).ready(function(){

		$('#register-event').on('click', function(){
			document.getElementById('register').style.display = 'block';
		});

		$('#cancel-register').on('click', function(){
			document.getElementById('register').style.display = 'none';
		});

	});

</script>

@include('partials.error2')

@include('partials.success2')

@endsection