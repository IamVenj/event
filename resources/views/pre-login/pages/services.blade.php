@extends('pre-login.index.index')

@section('content')

<style type="text/css">
	.page-header.background-image {
    background-image: url('{{URL::asset('storage/uploads/custom-pages/custom-main/main-img.jpg')}}');
    background-size: cover;
    background-position: center bottom;
    background-repeat: no-repeat; }
</style>

 <div class="main-wrapper">
    <div class="main">
        <div class="main-inner">
        	
            <div class="content">
                <div class="page-header pull-top background-image">
					<div class="page-header-inner">
						<div class="container-fluid">
						</div><!-- /.container-fluid -->
					</div><!-- /.page-header-inner -->
				</div><!-- /.page-header -->
                <div class="page-header pull-top page-header-simple">
					<div class="page-header-inner">
						<div class="container-fluid">
							<h1>Services</h1>
						</div><!-- /.container-fluid -->
					</div><!-- /.page-header-inner -->
				</div><!-- /.page-header -->

				<div class="container-fluid ">
					<ol class="breadcrumb">
						<li><a href="/">Home</a></li>
						<li class="active">Services</li>
					</ol>
				</div><!-- /.container-fluid -->

				<div class="boxes">
					<div class="container-fluid">
						@if(!is_null($services))

						@foreach($services as $service)

						<div class="box-wrapper">
							<div class="box">
								<i class="fa {{$service->icon}}"></i>
								<h2>{{$service->title}}</h2>

								<p>
									<?php echo $service->slug; ?>
								</p>
							</div><!-- /.box -->
						</div><!-- /.box-wrapper -->

						@endforeach

						@endif
					</div><!-- /.container-fluid -->
				</div><!-- /.boxes -->

            </div><!-- /.content -->
        </div><!-- /.main-inner -->
    </div><!-- /.main -->
</div><!-- /.main-wrapper -->

@endsection