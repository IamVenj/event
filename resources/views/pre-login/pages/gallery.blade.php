@extends('pre-login.index.index')

@section('content')

 <div class="main-wrapper">
    <div class="main">
        <div class="main-inner">
        
            <div class="content">
                <div class="page-header pull-top">
					<div class="page-header-inner">
						<div class="container-fluid">
							<h1>Gallery</h1>
						</div><!-- /.container-fluid -->
					</div><!-- /.page-header-inner -->
				</div><!-- /.page-header -->

				<div class="container-fluid">
					<ol class="breadcrumb">
						<li><a href="/">Home</a></li>
						<li><a href="/gallery">Gallery</a></li>
					</ol>
				</div><!-- /.container-fluid -->

				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-12 col-xl-12">
							
							<div class="row">	

								@if(!is_null($gallery))

								@foreach($gallery as $image)
										
								<div class="col-sm-3">
									<div class="card">
										<div class="card-inner">
											<div class="card-image">
												<a href="{{URL::asset('storage/app/public/uploads/gallery/'.$image->image)}}" title="{{$image->name}}" data-background="image" data-src="{{URL::asset('storage/app/public/uploads/gallery/'.$image->image)}}" data-lightbox="events" style="background-image: url('{{URL::asset('storage/app/public/uploads/gallery/'.$image->image)}}')">
													<span><i class="fa fa-search"></i></span>
												</a>

											</div><!-- /.card-image -->

											<div class="card-content">	
												<div class="card-date">
													<strong>{{date('d', strtotime($image->created_at))}}</strong>
													<span>{{date('y', strtotime($image->created_at))}}</span>
												</div><!-- /.card-date -->

												<h3 class="card-title">
													{{$image->name}}
												</h3>

												<h4 class="card-subtitle">
													{{$image->location}}
												</h4>
											</div><!-- /.card-content -->
										</div><!-- /.card-inner -->
									</div><!-- /.card -->
								</div><!-- /.col-* -->
							
								@endforeach

								@endif

				
							</div><!-- /.row -->
							<div class="center">
								<ul class="pagination">
									

									<li class="page-item"><a class="page-link" href="#">{{$gallery->links()}}</a></li>

									
								</ul><!-- /.pagination -->
							</div><!-- /.center -->
						</div><!-- /.col-* -->
					</div><!-- /.row -->
				</div><!-- /.container-fluid -->
            </div><!-- /.content -->
        </div><!-- /.main-inner -->
    </div><!-- /.main -->
</div><!-- /.main-wrapper -->

@endsection