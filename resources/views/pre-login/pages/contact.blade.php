@extends('pre-login.index.index')

@section('content')

<style type="text/css">
	.page-header.background-image {
    background-image: url('{{URL::asset('storage/app/public/uploads/custom-pages/custom-main/main-img.jpg')}}');
    background-size: cover;
    background-position: center bottom;
    background-repeat: no-repeat; }
</style>

<div class="main-wrapper">
    <div class="main">
        <div class="main-inner">
        	
            <div class="content">

            	<div class="page-header pull-top background-image">
					<div class="page-header-inner">
						<div class="container-fluid">
							<!-- <h1>Recent News From The City</h1> -->

							
						</div><!-- /.container-fluid -->
					</div><!-- /.page-header-inner -->
				</div><!-- /.page-header -->
                <div class="container-fluid">
				
					<iframe id="contact-map" class="bg-white" src="{{$setting->google_map_location}}" frameborder="0" style="border:0; width: 100%; margin-bottom: 50px; background: white; box-shadow: 1px 10px 20px rgba(35, 35, 35, 0.07);" allowfullscreen></iframe>
		
					<div class="row">
						<div class="col-sm-6">
							<h3>Get In Touch With Us</h3>


							<form method="post" action="/contact">
								@csrf
								<div class="form-group">
									<label>Name</label>
									<input type="text" name="name" class="form-control bordered">
								</div><!-- /.form-group-->

								<div class="form-group">
									<label>E-mail Address</label>
									<input type="email" name="email" class="form-control bordered">
								</div><!-- /.form-group-->
								<div class="form-group">
									<label>Subject</label>
									<input type="text" name="subject" class="form-control bordered">
								</div><!-- /.form-group-->

								<div class="form-group">
									<label>Message</label>
									<textarea class="form-control bordered" name="message"></textarea>
								</div><!-- /.form-group -->

								<button type="submit" class="btn btn-secondary pull-right">Post Message</button>
							</form>
						</div><!-- /.col-* -->

						<div class="col-sm-6">
							<!-- <h3>Aliquam sodales tincidunt ex, vitae sagittis magna congue vel. Vestibulum non hendrerit diam.</h3>

							<p class="large">
								Maecenas venenatis et ex at posuere. Nunc pulvinar orci vitae est condimentum, et scelerisque justo porta. Morbi accumsan sollicitudin orci et eleifend. Aliquam sodales tincidunt ex, vitae sagittis magna congue vel. Vestibulum non hendrerit diam.
							</p>
				 -->
							<h4 class="center">Contact Information</h4>

							<div class="dropdown-divider" style="margin-bottom: 30px;"></div>

							<div class="row">
								<div class="col-sm-6">
									<!-- <h5>San Francisco</h5> -->

									<h5>{{$setting->company_name}}</h5>
									<h5>{{$setting->location}}</h5>	
									
									

										
								</div><!-- /.row -->
								<div class="col-sm-6">

										

										<h4>
											<strong>E-mail:</strong> <a href="mailto:{{$setting->email}}">{{$setting->email}}</a></h4>
										<h4><strong>Phone:</strong><a href="tel://{{$setting->phone_number}}"> {{$setting->phone_number}}</a></h4>
										

											
								</div><!-- /.col -->
							</div><!--/.row-->
						</div><!-- /.col-* -->
					</div><!-- /.row -->
				</div><!-- /.container-fluid -->
            </div><!-- /.content -->
        </div><!-- /.main-inner -->
    </div><!-- /.main -->
</div><!-- /.main-wrapper -->


@include('partials.error2')

@include('partials.success2')

@endsection