@extends('pre-login.index.index')

@section('content')

 <div class="main-wrapper">
    <div class="main">
        <div class="main-inner">
        
            <div class="content">
                <div class="page-header pull-top">
					<div class="page-header-inner">
						<div class="container-fluid">
							<h1>{{$category_name}}</h1>
						</div><!-- /.container-fluid -->
					</div><!-- /.page-header-inner -->
				</div><!-- /.page-header -->

				<div class="container-fluid">
					<ol class="breadcrumb">
						<li><a href="/">Home</a></li>
						<li><a href="/{{$category_name}}/{{$id}}">{{$category_name}}</a></li>
					</ol>
				</div><!-- /.container-fluid -->

				<div class="container-fluid">
					<div class="row">
						

						<?php

						$events = App\Event::where('event_category_id', $id)->get();

						?>

						@foreach($events as $event)

						<div class="col-lg-12 col-xl-12">
							

							<div class="row">	
										
								<div class="col-sm-3">
									<div class="card">
										<div class="card-inner">
											<div class="card-image">
												<a href="/event/{{$event->id}}" style="background-image: url('{{URL::asset('storage/uploads/event/'.$event->image)}}');">
													<span><i class="fa fa-search"></i></span>
												</a>

												<div class="card-actions">
													<a href="#"><i class="fa fa-user"></i> </a><p style="color: #fff;">{{App\UserEvent::where('event_id', $event->id)->count()}}</p>
												</div><!-- /.card-actions -->
											</div><!-- /.card-image -->

											<div class="card-content">	
												<div class="card-date">
													<strong>{{date('d', strtotime($event->date_from))}}</strong>
													<span>{{date('M', strtotime($event->date_from))}}</span>
												</div><!-- /.card-date -->

												<div class="card-date">
													<strong>{{date('d', strtotime($event->date_to))}}</strong>
													<span>{{date('M', strtotime($event->date_from))}}</span>
												</div>

												<h3 class="card-title">
													<a href="/event/{{$event->id}}">{{$event->name}}</a>
												</h3>

												<h4 class="card-subtitle">
													<a href="/event/{{$event->id}}">{{$event->location}}</a>
												</h4>
											</div><!-- /.card-content -->
										</div><!-- /.card-inner -->
									</div><!-- /.card -->
								</div><!-- /.col-* -->
							
								
							</div><!-- /.row -->

							@endforeach

						</div><!-- /.col-* -->
					</div><!-- /.row -->
				</div><!-- /.container-fluid -->
            </div><!-- /.content -->
        </div><!-- /.main-inner -->
    </div><!-- /.main -->
</div><!-- /.main-wrapper -->

@endsection