@extends('pre-login.index.index')

@section('content')

<div class="main-wrapper">
    <div class="main">
        <div class="main-inner">
            <div class="content">
                <div class="hero hero-creative-wrapper">
					<div class="hero-creative">
						<div class="hero-creative-image" style="background-image: url({{URL::asset('storage/app/public/uploads/custom-pages/custom-main-event/main-event-img.jpg')}});">

							@if(!is_null($main_event->event_id))
                            
							<?php

							$selected_event = App\Event::find($main_event->event_id);
                            
							?>
                            
                            

							<div class="hero-creative-title">
								
								<h2>{{$selected_event->name}}</h2>
								<p style="font-weight: normal;"><?= $selected_event->slug; ?></p>
								<a href="/event/{{$selected_event->id}}" class="btn btn-secondary btn-lg">Register <i class="fa fa-chevron-right"></i></a>
								<!-- <form action="" id="quickSearchForm"> -->
								
							</div	><!-- /.hero-creative-title -->

							@else

							<div class="hero-creative-title">
								
								<h2>Promote Awesome City Events</h2>
								<p>Descripiton of the event</p>
								
							</div	><!-- /.hero-creative-title -->


							@endif

							<div class="hero-scroll">
								<div class="hero-scroll-mice">
								</div><!-- /.hero-scroll-mice -->
								<span>Scroll Down</span>
							</div><!-- /.hero-scroll -->								
						</div><!-- /.hero-image -->
					</div><!-- /.hero-creative -->
				</div><!-- /.hero -->

				<div class="boxes">
					<div class="container-fluid no-padding">

						@if(!is_null($services))

						@foreach($services as $service)

						<div class="box-wrapper">
							<div class="box">
								<i class="fa {{$service->icon}}"></i>
								<h2>{{$service->title}}</h2>

								<p>
									<?php echo $service->slug; ?>
								</p>
							</div><!-- /.box -->
						</div><!-- /.box-wrapper -->

						@endforeach

						@endif

						@if(App\Service::all()->count() > 4)

						<a href="/services">
							<div class="box-wrapper" style="width: 100%;">
								<div class="box">	
									<h5 class="center">Show More<i class="fa fa-caret-right" ></i></h5>
									<p>
									Would you like to see more of our services?
								</p>
								</div>
							</div>
						</a>	

						@endif
								
					</div><!-- /.container-fluid -->
				</div><!-- /.boxes -->


				<div class="container-fluid push-bottom">
					<div class="page-title">
						<h1 style="font-weight: bold;">Events</h1>
					</div><!-- /.page-title -->

					<div class="row">

						@foreach($events as $event)
	
						<div class="col-lg-4 col-xl-4 col-6">
							<div class="card">
								<div class="card-inner">
									<div class="card-image">

										@if(!is_null($event->image))

										<a href="/event/{{$event->id}}" style="background-image: url('{{URL::asset('storage/app/public/uploads/event/'.$event->image)}}');">
											<span><i class="fa fa-search"></i></span>
										</a>

										@else

										<a href="/event/{{$event->id}}" style="padding-top: 68%; border-top-left-radius: 10px; border-top-right-radius: 10px;  background: linear-gradient(120deg, #00e4d0, #429FFD);" alt="Card image cap"></a>

										@endif

									</div><!-- /.card-image -->

									<div class="card-content">	
										<div class="card-date">
											<strong>{{date('d', strtotime($event->date_from))}}</strong>
											<span>{{date('M', strtotime($event->date_from))}}</span>
										</div><!-- /.card-date -->

										<div class="card-date">
											<strong>{{date('d', strtotime($event->date_to))}}</strong>
											<span>{{date('M', strtotime($event->date_from))}}</span>
										</div>

										<h3 class="card-title">
											<a href="/event/{{$event->id}}">{{$event->name}}</a>
										</h3>

										<h4 class="card-subtitle">
											<a href="#">{{$event->location}}</a>
										</h4>
									</div><!-- /.card-content -->
								</div><!-- /.card-inner -->
							</div><!-- /.card -->
						</div><!-- /.col-* -->
		
						@endforeach
						
					</div><!-- /.row -->
					
				</div><!-- /.container-fluid -->

				<div class="partners">
					<div class="container-fluid">
						<div class="page-title page-title-fullwidth-description page-title-no-padding-top center">
							<h1 style="font-weight: bold;">Partners of {{$setting->company_name}}</h1>

							<h2>
								Brought to you by
							</h2>
						</div><!-- /.page-title -->
						<div class="row">
							
							@foreach($partners as $partner)

							<div class="col-md-2">
								<a>
									<img src="{{URL::asset('storage/app/public/uploads/partners/'.$partner->image)}}" alt="" height="50">
								</a>		
							</div>

							@endforeach

						</div>
						
					</div><!-- /.container -->
				</div><!-- /.partners -->

				<div class="cta-wrapper pull-bottom">
					<div class="cta">
						<div class="cta-map"></div><!-- /.cta-map -->
						
						<div class="cta-inner">
							<div class="container-fluid">
								<div class="cta-content-wrapper">
									<div class="cta-content">
										<h2>Do you want to register for an event?</h2>
										<h3>We will present you with unforgetable events</h3>
									</div><!-- /.cta-content -->
									
									<div class="cta-action">
										<a href="/register" class="btn btn-black btn-lg">Create Account Now</a>
									</div><!-- /.cta-action -->
								</div><!-- /.cta-content-wrapper -->				
								<!-- <div class="cta-image"></div> -->
								<!-- /.cta-image -->
							</div><!-- /.container-fluid -->
						</div><!-- /.cta-inner -->		
					</div><!-- /.cta -->
				</div><!-- /.cta-wrapper -->

            </div><!-- /.content -->
        </div><!-- /.main-inner -->
    </div><!-- /.main -->
</div><!-- /.main-wrapper -->

@endsection