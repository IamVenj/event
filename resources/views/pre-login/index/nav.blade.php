<div class="header-sticky">
	<div class="header-sticky-inner">
	    <div class="header-logo">
            <a href="/">
                <img src="{{URL::asset('storage/app/public/uploads/logo.png')}}" style="object-fit: contain;" alt="logo" />
            </a>
        </div><!-- /.header-logo -->

        <div class="nav-primary navbar-toggleable-md collapse">
			<ul class="nav nav-pills">
				<!-- <li class="nav-item has-children">
					<a href="#" class="nav-link active">Home</a>

					<ul class="sub-menu">
						<li><a href="index-2.html">Creative Hero</a></li>
						<li><a href="index-map.html">Google Map Hero</a></li>
					</ul>
				</li> -->
				<li class="nav-item"><a href="/" class="nav-link @if(\Request::is('/')) active @endif">Home</a></li>
				<!-- <li class="nav-item"><a href="/events" class="nav-link @if(\Request::is('events')) active @endif ">Events</a></li> -->
				<!-- <li class="nav-item"><a href="/popular" class="nav-link @if(\Request::is('popular')) active @endif ">Popular</a></li> -->
				<!-- <li class="nav-item"><a href="/services" class="nav-link @if(\Request::is('services')) active @endif ">Services</a></li> -->
				<li class="nav-item"><a href="/gallery" class="nav-link @if(\Request::is('gallery')) active @endif ">Gallery</a></li>
				<li class="nav-item"><a href="/contact" class="nav-link @if(\Request::is('contact')) active @endif ">Contact</a></li>
				@auth
				<li class="nav-item has-children">
					<a href="#" class="nav-link ">{{auth()->user()->name}}</a>

					<ul class="sub-menu">
						@if(auth()->user()->role_id == 2)
							<li><a href="/profile">Profile</a></li>
							<li><a href="/update-password">Change Password</a></li>
							<li><a href="/my-booking">My Bookings</a></li>
						@endif
						<li><a href="/logout">Logout</a></li>
					</ul>
				</li>
				@endauth
			</ul>
		</div><!-- /.nav-primary -->

		<button class="navbar-toggler hidden-lg-up" type="button" data-toggle="collapse" data-target=".header .nav-primary">
			<span></span>
			<span></span>
			<span></span>
		</button>	
	</div><!-- /.header-sticky-inner -->
</div><!-- /.header-sticky -->

<div class="page-wrapper">
	<div class="header-wrapper">
		<div class="header">		
			<div class="header-inner">
				<div class="header-top">
					<div class="container-fluid">
						<div class="header-logo">
							<a href="/">
								 <img src="{{URL::asset('storage/app/public/uploads/logo.png')}}" style="object-fit: contain;" alt="logo" />
								<strong>
									<span>{{$setting->company_name}}</span>
								</strong>
							</a>
						</div><!-- /.header-logo -->

						<div class="header-toggle sidenav-trigger">
						</div><!-- /.header-toggle -->

					</div><!-- /.container-fluid -->
				</div><!-- /.header-top -->
				<div class="dropdown-divider" style="width: 50%; margin-top: 0;"></div>
				<div class="header-bottom">
					<div class="container-fluid">
						<div class="nav-primary navbar-toggleable-md collapse">
							<ul class="nav nav-pills">
								<!-- <li class="nav-item has-children">
									<a href="#" class="nav-link active">Home</a>

									<ul class="sub-menu">
										<li><a href="index-2.html">Creative Hero</a></li>
										<li><a href="index-map.html">Google Map Hero</a></li>
									</ul>
								</li> -->
								<li class="nav-item"><a href="/" class="nav-link  @if(\Request::is('/')) active @endif">Home</a></li>
								<!-- <li class="nav-item"><a href="/events" class="nav-link  @if(\Request::is('events')) active @endif">Events</a></li> -->
								<!-- <li class="nav-item"><a href="/popular" class="nav-link @if(\Request::is('popular')) active @endif ">Popular</a></li> -->
								<!-- <li class="nav-item"><a href="/services" class="nav-link @if(\Request::is('services')) active @endif ">Services</a></li> -->
								<li class="nav-item"><a href="/gallery" class="nav-link @if(\Request::is('/gallery')) active @endif ">Gallery</a></li>
								<li class="nav-item"><a href="/contact" class="nav-link @if(\Request::is('contact')) active @endif ">Contact</a></li>
								<div class="toggled-view">
									<div class="dropdown-divider"></div>
									@guest
									<li class="nav-item"><a href="/login" class="nav-link"><i class="fa fa-sign-in" style="margin-right: 5px;"></i>Login</a></li>
									<li class="nav-item"><a href="/register" class="nav-link"><i class="fa fa-user-plus" style="margin-right: 5px;"></i>Sign Up</a></li>
									@else
									<li class="nav-item has-children">
										<a href="#" class="nav-link ">{{auth()->user()->name}}</a>

										<ul class="sub-menu">
											@if(auth()->user()->role_id == 2)
											<li><a href="/profile">Profile</a></li>
											<li><a href="/update-password">Change Password</a></li>
											<li><a href="/my-booking">My Bookings</a></li>
											@endif
											<li><a href="/logout">Logout</a></li>
										</ul>
									</li>
									@endguest
								</div>
							</ul>
						</div><!-- /.nav-primary -->

						<button class="navbar-toggler hidden-lg-up" type="button" data-toggle="collapse" data-target=".header .nav-primary">
							<span></span>
							<span></span>
							<span></span>
						</button>	

						<ul class="nav nav-social nav-right">
							
							@if(!is_null($setting->facebook))
							<li class="nav-item"><a href="{{$setting->facebook}}}" class="nav-link"><i class="fa fa-facebook"></i></a></li>
							@endif

							@if(!is_null($setting->twittter))
							<li class="nav-item"><a href="{{$setting->twittter}}" class="nav-link"><i class="fa fa-twitter"></i></a></li>
							@endif

							@if(!is_null($setting->google_plus))
							<li class="nav-item"><a href="{{$setting->google_plus}}" class="nav-link"><i class="fa fa-google-plus"></i></a></li>
							@endif

							@if(!is_null($setting->linked_in))
							<li class="nav-item"><a href="{{$setting->linked_in}}" class="nav-link"><i class="fa fa-linkedin"></i></a></li>
							@endif

						</ul>

						<ul class="nav nav-pills nav-secondary nav-right">
							@guest
							<li class="nav-item"><a href="/login" class="nav-link"><i class="fa fa-sign-in" style="margin-right: 5px;"></i>Login</a></li>
							<li class="nav-item"><a href="/register" class="nav-link"><i class="fa fa-user-plus" style="margin-right: 5px;"></i>Sign Up</a></li>
							@else
							<li class="nav-item has-children">
								<a href="#" class="nav-link ">{{auth()->user()->name}}</a>

								<ul class="sub-menu">
									@if(auth()->user()->role_id == 2)
										<li><a href="/profile">Profile</a></li>
										<li><a href="/update-password">Change Password</a></li>
										<li><a href="/my-booking">My Bookings</a></li>
									@endif
									<li><a href="/logout">Logout</a></li>
								</ul>
							</li>
							@endguest

						</ul>					
					</div><!-- /.container-fluid -->
				</div><!-- /.header-bottom -->
			</div><!-- /.header-inner -->
		</div><!-- /.header -->
	</div><!-- /.header-wrapper -->