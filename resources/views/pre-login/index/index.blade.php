<?php

$setting = App\CompanySettings::first();

?>
<!DOCTYPE html>
<html>


<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <link href="{{URL::asset('assets/libraries/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{URL::asset('assets/libraries/owl-carousel/owl.carousel.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{URL::asset('assets/libraries/owl-carousel/owl.carousel.default.css')}}" rel="stylesheet" type="text/css">
    <link href="{{URL::asset('assets/css/eve.css')}}" rel="stylesheet" type="text/css" id="css-primary">
    <link href="{{URL::asset('css/lightbox.css')}}" rel="stylesheet" type="text/css" id="css-primary">
	<link rel="shortcut icon" href="{{URL::asset('storage/app/public/uploads/favicon.png')}}" />
  	<!-- <link rel="shortcut icon" href="assets/img/logo.png" /> -->

    <title>{{$setting->company_name}}</title>
</head>

<body>


@include('pre-login.index.nav')

@yield('content')

@include('pre-login.index.footer')

@include('pre-login.index.upcoming_component')

<script src="http://maps.googleapis.com/maps/api/js" type="text/javascript"></script>

<script src="{{ URL::asset('vendors/js/vendor.bundle.base.js') }}"></script>

<script src="{{ URL::asset('vendors/js/vendor.bundle.addons.js') }}"></script>

<script type="text/javascript" src="{{URL::asset('assets/js/jquery.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/lightbox.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('assets/js/tether.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('assets/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('assets/libraries/bootstrap-typeahead/bootstrap3-typeahead.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('assets/libraries/owl-carousel/owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('assets/js/jquery.gmap3.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('assets/js/jquery.ezmark.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('assets/js/eve.js')}}"></script>

</body>

</html>