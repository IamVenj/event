	<div class="footer-wrapper">
		<div class="footer">
			<div class="container-fluid">
				<div class="footer-inner">
					<div class="footer-top">
						<div class="footer-left">
							<p class="">&copy; {{$setting->company_name}}. All rights reserved.</p>
						</div><!-- /.footer-left -->

						<div class="footer-right">
							<ul class="nav nav-social">
								@if(!is_null($setting->facebook))
								<li class="nav-item"><a href="{{$setting->facebook}}}" class="nav-link"><i class="fa fa-facebook"></i></a></li>
								@endif

								@if(!is_null($setting->twittter))
								<li class="nav-item"><a href="{{$setting->twittter}}" class="nav-link"><i class="fa fa-twitter"></i></a></li>
								@endif

								@if(!is_null($setting->google_plus))
								<li class="nav-item"><a href="{{$setting->google_plus}}" class="nav-link"><i class="fa fa-google-plus"></i></a></li>
								@endif

								@if(!is_null($setting->linked_in))
								<li class="nav-item"><a href="{{$setting->linked_in}}" class="nav-link"><i class="fa fa-linkedin"></i></a></li>
								@endif
							</ul>					
						</div><!-- /.footer-right -->
					</div><!-- /.footer-top -->
				</div><!-- /.footer-inner -->
			</div><!-- /.container-fluid -->
		</div><!-- /.footer -->
	</div><!-- /.footer-wrapper -->

	


<div class="page-wrapper-overlay"></div><!-- /.page-wrapper-overlay -->
</div><!-- /.page-wrapper -->