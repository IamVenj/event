<?php

$_list_events_ = App\Event::orderBy('created_at', 'desc')->get()->take(4);

?>
<div class="side-navigation">
	<div class="widget">
		<h2 class="widgettitle">Upcoming Events</h2>
	</div><!-- /.widget -->

	<ul class="timeline">

		@foreach($_list_events_ as $_list_event_)

		<li>
			<h3><a href="/event/{{$_list_event_->id}}">{{$_list_event_->name}}</a></h3>

			<div class="clearfix">
				<div class="timeline-image">
					<a href="/event/{{$_list_event_->id}}" style="background-image: url('{{URL::asset('storage/app/public/uploads/event/'.$_list_event_->image)}}');"></a>
				</div><!-- /.timeline-image -->

				<div class="timeline-attrs">
					<span>{{$_list_event_->location}}</span>
					<span>Price: <strong>{{$_list_event_->price}}</strong></span>
				</div><!-- /.timeline-attrs -->
			</div><!-- /.clearfix -->

			<p><?= str_limit($_list_event_->slug, 50); ?></p>
		</li>
	
		@endforeach
		
	</ul>

	<!-- <a href="#" class="btn btn-secondary btn-block">Show Booked Events</a> -->
</div><!-- /.side-navigation -->