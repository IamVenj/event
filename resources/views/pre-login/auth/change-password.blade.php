@extends('pre-login.index.index')

@section('content')

<div class="main-wrapper">
    <div class="main">
        <div class="main-inner">
            <div class="content" style="padding-bottom:  0px;">
                <div class="hero hero-creative-wrapper">
					<div class="hero-creative">
						<div class="hero-creative-image" style="background-image: url('{{URL::asset('storage/app/public/uploads/custom-pages/custom-auth/auth-img.jpg')}}');">
							<div class="hero-creative-title">
								<h2>Change Password</h2>

								<form action="/update-password" method="post" class="hero-creative-search" style="margin-top: 0px;">
									@csrf
									@METHOD('PATCH')
									<fieldset style="border-bottom-left-radius: 50px; border-top-right-radius: 50px; padding-top: 30px; padding-bottom: 30px;">
												<legend>Update your password</legend>

												<div class="row">
												    
												    <div class="col-sm-6">
												        <div class="form-group">
												            <label>Current Password</label>	
												            <input type="password" class="form-control " name="current_password" placeholder="current password" style="border-radius: 5px; box-shadow: 1px 10px 10px rgba(0,0,0,0.05); padding: 25px;">
												        </div><!-- /.form-group -->
												    </div><!-- /.col-* -->
												     <div class="col-sm-6">
												        <div class="form-group">
												            <label>New Password</label>
												            <input type="password" class="form-control " name="new_password" placeholder="new password" style="border-radius: 5px; box-shadow: 1px 10px 10px rgba(0,0,0,0.05); padding: 25px;">
												        </div><!-- /.form-group -->
												    </div><!-- /.col-* -->
												</div>
												<button class="btn btn-success" type="submit" style="margin-top: 30px; border-radius: 3px; box-shadow: 1px 5px 20px rgba(35, 35, 35, 0.2)"><i class="fa fa-upload" style="margin-right: 5px;"></i>Update</button>	
											</fieldset>

								</form>
							</div	><!-- /.hero-creative-title -->

													
						</div><!-- /.hero-image -->
					</div><!-- /.hero-creative -->
				</div><!-- /.hero -->
			</div>
		</div>
	</div>
</div>

@include('partials.error2')
@include('partials.success2')

@endsection