@extends('pre-login.index.index')

@section('content')

<div class="main-wrapper">
    <div class="main">
        <div class="main-inner">
            <div class="content" style="padding-bottom:  0px;">
                <div class="hero hero-creative-wrapper">
					<div class="hero-creative">
						<div class="hero-creative-image" style="background-image: url('{{URL::asset('storage/app/public/uploads/custom-pages/custom-auth/auth-img.jpg')}}');">
							<div class="hero-creative-title">
								<h2>Sign Up</h2>
								<!-- <form action="" id="quickSearchForm"> -->
								<!-- <div class="dropdown-divider" style="margin-top: 30px;"></div> -->

								<!-- <h1 style="margin-top: 30px;">Find Nearby Events in Ethiopia</h1> -->

								<form action="/register" method="post" class="hero-creative-search" style="margin-top: 30px;">
									@csrf
									<fieldset style="border-bottom-left-radius: 50px; border-top-right-radius: 50px;">
												<!-- <legend>Contact Information</legend> -->

												<div class="row">
												    <div class="col-sm-4">
												        <div class="form-group">
												            <label>E-mail</label>
												            <input type="email" name="email" placeholder="E-mail" class="form-control " style="border-radius: 5px; box-shadow: 1px 10px 10px rgba(0,0,0,0.05); padding: 25px;">
												        </div><!-- /.form-group -->
												    </div><!-- /.col-* -->

												    <div class="col-sm-8">
												        <div class="form-group">
												            <label>Full Name</label>
												            <input type="text" name="full_name" class="form-control " placeholder="full name" style="border-radius: 5px; box-shadow: 1px 10px 10px rgba(0,0,0,0.05); padding: 25px;">
												        </div><!-- /.form-group -->
												    </div><!-- /.col-* -->

												    <div class="col-sm-6">
												        <div class="form-group">
												            <label>Gender</label>
												           <select class="form-control form-control-lg" name="gender" style="border-radius: 5px; box-shadow: 1px 10px 10px rgba(0,0,0,0.05); ">
									                          <option selected disabled>Select Gender</option>
									                          <option value="m">Male</option>
									                          <option value="f">Female</option>
									                        </select>
												        </div><!-- /.form-group -->
												    </div><!-- /.col-* -->

												    <div class="col-sm-6">
												        <div class="form-group">
												            <label>Phone Number</label>
												            <input type="text" name="phone_number" class="form-control " placeholder="phone number" style="border-radius: 5px; box-shadow: 1px 10px 10px rgba(0,0,0,0.05); padding: 25px;">
												        </div><!-- /.form-group -->
												    </div><!-- /.col-* -->

												    <div class="col-sm-6">
												        <div class="form-group">
												            <label>Password</label>	
												            <input type="password" name="password" class="form-control " placeholder="password" style="border-radius: 5px; box-shadow: 1px 10px 10px rgba(0,0,0,0.05); padding: 25px;">
												        </div><!-- /.form-group -->
												    </div><!-- /.col-* -->
												     <div class="col-sm-6">
												        <div class="form-group">
												            <label>Confirm Password</label>
												            <input type="password" name="password_confirmation" class="form-control " placeholder="confirm password" style="border-radius: 5px; box-shadow: 1px 10px 10px rgba(0,0,0,0.05); padding: 25px;">
												        </div><!-- /.form-group -->
												    </div><!-- /.col-* -->
												</div>
												<button class="btn btn-secondary btn-block btn-lg" type="submit" style="margin-top: 30px;"><i class="fa fa-user-plus" style="margin-right: 5px;"></i>Sign Up</button>	
											</fieldset>

								</form>
							</div	><!-- /.hero-creative-title -->

													
						</div><!-- /.hero-image -->
					</div><!-- /.hero-creative -->
				</div><!-- /.hero -->
			</div>
		</div>
	</div>
</div>

@include('partials.error2')

@include('partials.success2')

@endsection