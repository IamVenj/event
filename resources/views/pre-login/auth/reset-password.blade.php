@extends('pre-login.index.index')

@section('content')

<div class="main-wrapper">
    <div class="main">
        <div class="main-inner">
            <div class="content" style="padding-bottom:  0px;">
                <div class="hero hero-creative-wrapper">
					<div class="hero-creative">
						<div class="hero-creative-image" style="background-image: url('{{URL::asset('storage/app/public/uploads/custom-pages/custom-auth/auth-img.jpg')}}');">
							<div class="hero-creative-title">
								
								<div class="col-xl-8 col-sm-10">
								
								<h2>Reset Password</h2>

								<form action="/reset-password" method="post" class="hero-creative-search" style="margin-top: 30px;">

									<fieldset style="border-bottom-left-radius: 50px; border-top-right-radius: 50px;">

										<input type="hidden" name="token" value="{{ $token }}">

										<div class="row">
										   
										    <div class="col-sm-12">
										        <div class="form-group">
										            <label>E-mail</label>
										            <input type="email" placeholder="E-mail" class="form-control " name="email" style="border-radius: 5px; box-shadow: 1px 10px 10px rgba(0,0,0,0.05); padding: 25px;">
										        </div><!-- /.form-group -->
										    </div><!-- /.col-* -->
										   
										    <div class="col-sm-12">
										        <div class="form-group">
										            <label>Password</label>
										            <input type="password" placeholder="Password" name="password" class="form-control " style="border-radius: 5px; box-shadow: 1px 10px 10px rgba(0,0,0,0.05); padding: 25px;">
										        </div><!-- /.form-group -->
										    </div><!-- /.col-* -->
										   
										    <div class="col-sm-12">
										        <div class="form-group">
										            <label>Confirm Password</label>
										            <input type="password" placeholder="Confirm password" name="password_confirmation" class="form-control " style="border-radius: 5px; box-shadow: 1px 10px 10px rgba(0,0,0,0.05); padding: 25px;">
										        </div><!-- /.form-group -->
										    </div><!-- /.col-* -->

										</div>
										<button class="btn btn-secondary btn-lg btn-block" type="submit" style="margin-top: 30px;"><i class="fa fa-send" style="margin-right: 5px;"></i>Send</button>	
									</fieldset>

								</form>
							</div	><!-- /.hero-creative-title -->
							</div	><!-- /.hero-creative-title -->
													
						</div><!-- /.hero-image -->
					</div><!-- /.hero-creative -->
				</div><!-- /.hero -->
			</div>
		</div>
	</div>
</div>

@include('partials.error2')
@include('partials.success2')

@endsection