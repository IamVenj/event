@extends('post-login.index.header')

@section('content')

@include('partials.error2')

@include('partials.success2')

<div class="main-panel">

	<div class="content-wrapper">

	  	<div class="row">

	    	<div class="col-md-12 d-flex align-items-stretch grid-margin">

	      		<div class="row flex-grow">

	        		<div class="col-12">

	          			<div class="card">

	            			<div class="card-body">

	            				<div class="row">

	            					<div class="col-md-10">

	              						<h4 class="card-title">Add Image</h4>

	            					</div>

	            					<div class="col-md-2">

			              				<a class="btn btn-primary btn-block" href="/admin-gallery" style="color: #fff;">		

							                <i class="mdi mdi-eye"></i> View Gallery

							            </a>

	            					</div>

	            				</div>

	            				<div class="dropdown-divider w-25"></div>

              					<p class="card-description mt-2 mb-2">

                					Add Image

              					</p>

              					<div class="dropdown-divider w-25 "></div>

              					<form class="forms-sample mt-4" action="/add-image" method="post" enctype="multipart/form-data">

              						@csrf

              						<div class="form-group">

					                  	<label for="title" style="font-size: 15px;">File Name<small style="color: red;">*</small></label>

					                  	<input type="text" class="form-control" name="name" id="title" placeholder="File Name" style="font-size: 15px;" required="">

					                </div>

					                <div class="form-group">

					                  	<label for="title" style="font-size: 15px;">Location<small style="color: red;">*</small></label>

					                  	<input type="text" class="form-control" name="location" id="title" placeholder="Location" style="font-size: 15px;" required="">

					                </div>


					                <div class="form-group" id="image">

					                  	<label for="file" style="font-size: 15px;">Image File <small style="color: red;">*</small></label>

					                  	<input type="file" name="image" class="form-control" id="file" placeholder="file" style="font-size: 15px;">

					                </div>
					                
					                <button type="submit" class="btn btn-primary mr-2 mt-4"><i class="mdi mdi-plus"></i>Add</button>

              					</form>

	            			</div>

	          			</div>

	        		</div>

	    		</div>

			</div>

		</div>

	</div>

	@include('post-login.index.footer')

</div>

<script src="js/jquery.min.js"></script>


@endsection