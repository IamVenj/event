@extends('post-login.index.header')

@section('content')

@include('partials.error2')

@include('partials.success2')

<div class="main-panel">

	<div class="content-wrapper">

	  	<div class="row">

	    	<div class="col-md-12 d-flex align-items-stretch grid-margin">

	      		<div class="row flex-grow">

	        		<div class="col-12">

	          			<div class="card">

	            			<div class="card-body">

	            				<h4 class="card-title">Custom-Auth</h4>

	            				<div class="dropdown-divider w-25"></div>

              					<p class="card-description mt-2 mb-2">

                					Customize Auth Pages

              					</p>

              					<div class="dropdown-divider w-25 "></div>

              					<form class="forms-sample mt-4" action="/custom-auth" method="post" enctype="multipart/form-data">
					                
              						@csrf

					                <div class="form-group">
					                  	<label for="image" style="font-size: 15px;">Main Login & signup Image <small style="color: red;">*</small></label>
					                  	<input type="file" name="image" class="form-control" id="image" placeholder="Main Service Image" style="font-size: 15px; font-weight: bold;">
					                </div>
					                
					                <label style="font-size: 15px; font-weight: bold">Current login & signup Image <i class="mdi mdi-arrow-down"></i></label>
					                
					                <div class="form-group">

					                  	<img src="{{URL::asset('storage/app/public/uploads/custom-pages/custom-auth/auth-img.jpg')}}" class="img-card mt-4" style="width: auto; height: 200px;">
					                </div>
					                
					                <button type="submit" class="btn btn-primary mr-2 mt-4"><i class="mdi mdi-cloud-download"></i>Update</button>

              					</form>

	            			</div>

	          			</div>

	        		</div>

	    		</div>

			</div>

		</div>

	</div>

	@include('post-login.index.footer')

</div>

@endsection