@extends('post-login.index.header')

@section('content')

@include('partials.error2')

@include('partials.success2')

<div class="main-panel">

	<div class="content-wrapper">

	  	<div class="row">

	    	<div class="col-md-12 d-flex align-items-stretch grid-margin">

	      		<div class="row flex-grow">

	        		<div class="col-12">

	          			<div class="card">

	            			<div class="card-body">

	            				<h4 class="card-title">Custom-Main Event</h4>

	            				<div class="dropdown-divider w-25"></div>

              					<p class="card-description mt-2 mb-2">

                					Customize Main Event

              					</p>

              					<div class="dropdown-divider w-25 "></div>

              					<form class="forms-sample mt-4" action="/custom-main-event" method="post" enctype="multipart/form-data">
					                @csrf

					                <div class="form-group">
					                
					                  	<label for="image" style="font-size: 15px;">Event <small style="color: red;">*</small></label>
					                
					                	<select class="form-control" style="font-size: 16px;" name="event">

					                		<option selected="" disabled="">Select Event</option>
					                	
					                		@foreach($events as $event)

					                		<option value="{{$event->id}}" <?php if($event->id == $main_event->event_id): ?> selected <?php endif; ?>>{{$event->name}}</option>

					                		@endforeach
					                
					                	</select>
					                
					                </div>

					                <div class="form-group">
					                
					                  	<label for="image" style="font-size: 15px;">Main Event Image <small style="color: red;">*</small></label>
					                
					                  	<input type="file" name="image" class="form-control" id="image" placeholder="Main Courses Image" style="font-size: 15px; font-weight: bold;">
					                
					                </div>
					                
					                <label style="font-size: 15px; font-weight: bold">Current Event Image <i class="mdi mdi-arrow-down"></i></label>

					                <div class="form-group">

					                  	<img src="{{URL::asset('storage/app/public/uploads/custom-pages/custom-main-event/main-event-img.jpg')}}" class="img-card mt-4" style="width: auto; height: 200px;">

					                </div>
					                
					                <button type="submit" class="btn btn-primary mr-2 mt-4"><i class="mdi mdi-cloud-download"></i>Update</button>

              					</form>

	            			</div>

	          			</div>

	        		</div>

	    		</div>

			</div>

		</div>

	</div>

	@include('post-login.index.footer')

</div>

@endsection