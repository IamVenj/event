@extends('post-login.index.header')

@section('content')

@include('partials.error2')

@include('partials.success2')

<div class="main-panel">

	<div class="content-wrapper">

	  	<div class="row">

			<div class="col-lg-12 grid-margin stretch-card">

        <div class="card">

          <div class="card-body">

            <h4 class="card-title" style="font-size:17px; text-transform:uppercase;"><b>Registered Personnel List</b></h4>

            <div class="dropdown-divider mb-4" style="width:25.5%;"></div>

            <div class="table-responsive">

              <table class="table table-hover" id="dataTables-example">

                <thead>

                  <tr>

                    <th style="font-size: 18px;">Full Name</th>
                    <th style="font-size: 18px;">Email</th>
                    <th style="font-size: 18px;">Phone Number</th>
                    <th style="font-size: 18px;">Gender</th>

                  </tr>

                </thead>

                <tbody>

                  @foreach($_user_event_ as $eu)

                  <?php

                  $users = App\User::where('id', $eu->user_id)->get();

                  ?>

                  @foreach($users as $user)

                  <tr class="record">

                    <td style="font-size: 15px;">{{$user->name}}</td>
                    <td style="font-size: 15px;">{{$user->email}}</td>
                    <td style="font-size: 15px;">{{$user->phone_number}}</td>
                    <td style="font-size: 15px;">@if($user->gender == 'm') Male @elseif($user->gender == 'f') Female @endif</td>


                  </tr>

                  @endforeach

                  @endforeach

                </tbody>

              </table>

            </div>

          </div>

        </div>

      </div>

		</div>

	</div>

	@include('post-login.index.footer')

</div>

<script src="js/jquery.min.js"></script>

<script>

$(document).ready(function() {

  $('#dataTables-example').DataTable({

    responsive: true

  });

});

</script> 


@endsection