@extends('post-login.index.header')

@section('content')

@include('partials.error2')
@include('partials.success2')

<div class="main-panel">

	<div class="content-wrapper">

	  	<div class="row">

	    	<div class="col-md-12 d-flex align-items-stretch grid-margin">

	      		<div class="row flex-grow">

	        		<div class="col-12">

	          			<div class="card">

	            			<div class="card-body">

	            				<div class="row">

	            					<div class="col-md-10">

	              						<h4 class="card-title">Event</h4>

	            					</div>

	            					<div class="col-md-2">

			              				<a class="btn btn-primary btn-block" href="event-admin" style="color: #fff;">		
							                
							                <i class="mdi mdi-eye"></i> View Event

							            </a>

	            					</div>

	            				</div>

	            				<div class="dropdown-divider w-25"></div>

              					<p class="card-description mt-2 mb-2">

                					Customize/Add your Event

              					</p>

              					<div class="dropdown-divider w-25 "></div>

              					<form class="forms-sample mt-4" action="/add-event" method="post" enctype="multipart/form-data">

              						@csrf

					                <div class="form-group">

					                  	<label for="name" style="font-size: 15px;">Event Name <small style="color: red;">*</small></label>

					                  	<input type="text" class="form-control" name="name" id="name" placeholder="event name" style="font-size: 15px;">

					                </div>

					                <!-- <div class="form-group">

					                  	<label for="category" style="font-size: 15px;">Event Category <small style="color: red;">*</small></label>

					                  	<select class="form-control" style="font-size: 17px;" name="event_category_id">

					                  		<option value="" selected="" disabled="">Please select an event category</option>
					                  		
					                  		{{-- @foreach($category as $event_category) --}}
					                  		
					                  		{{-- <option value="{{$event_category->id}}">{{$event_category->name}}</option> --}}
					                  		
					                  		{{-- @endforeach --}}
					                	
					                	</select>

					                </div> -->

					                <div class="row">
					                	
					                	<div class="col-md-4">					                		

							                <div class="form-group">
								                <label for="dtp_input2" class="control-label">Date: from</label>
								                <div class="input-group date form_date" data-date="" data-date-format="d/m/yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
								                    <input class="form-control" size="16" type="text" value="" name="date_from" readonly style="font-size: 16px;">
								                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
													<!-- <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span> -->
								                </div>
												<input type="hidden" id="dtp_input2" value="" /><br/>
								            </div>

					                	</div>

					                	<div class="col-md-4">
					                		
					                		<div class="form-group">
								                <label for="dtp_input2" class="control-label">Date: to</label>
								                <div class="input-group date form_date" data-date="" data-date-format="d/m/yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
								                    <input class="form-control" size="16" type="text" name="date_to" value="" readonly style="font-size: 16px;">
								                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
													<!-- <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span> -->
								                </div>
												<input type="hidden" id="dtp_input2" value="" /><br/>
								            </div>

					                	</div>

					                	<div class="col-md-4">
					                		
					                		<div class="form-group">
								                <label for="dtp_input3">Trip Starts</label>
								                <div class="input-group date form_time" data-date="" data-date-format="hh:ii:ss" data-link-field="dtp_input3" data-link-format="hh:ii:ss">
								                    <input class="form-control" size="16" type="text" value="" name="time" readonly style="font-size: 16px;">
								                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
													<!-- <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span> -->
								                </div>
												<input type="hidden" id="dtp_input3" value="" /><br/>
								            </div>

					                	</div>

					                </div>

					                <div class="form-group">

					                  	<label for="image" style="font-size: 15px;">Image</label>

					                  	<input type="file" name="image" class="form-control" id="image" placeholder="Image" style="font-size: 15px; font-weight: bold;">

					                </div>

					                <div class="form-group">

					                  	<label for="slug" style="font-size: 15px;">slug <small style="color: red;">*</small></label>

					                  	<textarea class="form-control"  rows="8" name="slug" id="slug" placeholder="Description/slug" style="font-size: 15px;" required></textarea>

					                </div>

					                <div class="form-group">

					                  	<label for="location" style="font-size: 15px;">Location <small style="color: red;">*</small></label>

					                  	<input type="text" class="form-control" name="location" id="location" placeholder="Location" style="font-size: 15px;">

					                </div>

					                <div class="form-group">

					                  	<label for="map_location" style="font-size: 15px;">Map Location <small style="color: red;">*</small></label>

					                  	<input type="text" class="form-control" name="map_location" id="map_location" placeholder="Map Location" style="font-size: 15px;">

					                </div>

					                <div class="form-group">

					                  	<label for="price" style="font-size: 15px;">Price <small style="color: red;">*</small></label>

					                  	<input type="text" class="form-control" name="price" id="price" placeholder="Price" style="font-size: 15px;">

					                </div>					                
					                
					                <button type="submit" class="btn btn-primary mr-2 mt-4"><i class="mdi mdi-plus"></i>Add</button>

              					</form>

	            			</div>

	          			</div>

	        		</div>

	    		</div>

			</div>


		</div>

	</div>

	@include('post-login.index.footer')

</div>

<script src="{{URL::asset('js/jquery.min.js')}}"></script>
 
<script src="{{ URL::asset('js/bootstrap-datetimepicker.min.js') }}"></script>

<script type="text/javascript">

  $(document).ready(function() {

    $("#slug").wysihtml5();

  });

  $('.form_date').datetimepicker({
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0
    });

  $('.form_time').datetimepicker({
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 1,
		minView: 0,
		maxView: 1,
		forceParse: 0,
    });

</script>

@endsection